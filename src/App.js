import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Dealers from "./pages/Dealers";
import Financing from "./pages/Financing";
import Listing from "./pages/Listing";
import Promotions from "./pages/Promotions";
import Shop from "./pages/Shop";
import Advancesearch from "./pages/Advancesearch";
//import Loader from './components/Loader';
import FAQ from "./pages/FAQ";
import PrivacyPolicy from "./pages/PrivacyPolicy";
import TermsOfUse from "./pages/TermsOfUse";
import ContactUs from "./pages/ContactUs";
import Advertise from "./pages/Advertise";
import Careers from "./pages/Careers";
import OurPartners from "./pages/OurPartners";
import PlaceAnAd from "./pages/PlaceAnAd";
import PostAd from "./pages/PostAd";
import Newsone from "./pages/Newsone";
import Newstwo from "./pages/Newstwo";
import Newsthree from "./pages/Newsthree";
import LoginSignup from "./pages/LoginSignup";
import Forgotpassword from "./pages/Forgotpassword";
import Changepassword from "./pages/Changepassword";
import OTPRegindv from "./pages/OTPRegindv";
import RegisterIndv from "./pages/RegisterIndv";

function App() {
  const logo1 =
    "http://cararabiyaweb.ahalfa.com/Content/assets/media/general/logo-light.png";
  return (
    <Router>
      <div>
        <PlaceAnAd />

        <header className="header">
          <div className="header-main mt-2 pb-2">
            <div className="container-fluid">
              <div className="row nav-margin-b">
                <div className="logo col-lg-2 col-auto">
                  <Link className="navbar-brand scroll " to="/">
                    <img
                      className="normal-logo"
                      src="./assets/media/logo-white.png"
                      alt="logo"
                    />
                  </Link>
                </div>
                <div className="d-flex align-self-center">
                  <button className="menu-mobile-button js-toggle-mobile-slidebar toggle-menu-button">
                    <i className="toggle-menu-button-icon">
                      <span />
                      <span />
                      <span />
                      <span />
                      <span />
                      <span />
                    </i>
                  </button>
                </div>
                <div className="container-fluid">
                  <div id="nav-main" className="row nav">
                    <div className="col-lg-2 d-flex align-items-center justify-content-start">
                      <Link className="navbar-brand scroll" to="/">
                        <img
                          className="normal-logo"
                          src="./assets/media/logo-white.png"
                          alt="logo"
                        />
                      </Link>
                    </div>
                    <div className="col-lg-6">
                      <nav
                        className="navbar navbar-dark navbar-expand-lg justify-content-left"
                        id="nav"
                      >
                        <ul className="yamm main-menu navbar-nav">
                          <li className="nav-item active">
                            <Link className="nav-link" to="/Listing">
                              Used Cars
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link" to="/inventory-list-new">
                              New Cars
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link" to="/inventory-list-new">
                              Certified Cars
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link" to="/promotions">
                              Offers &amp; Promotions
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link" to="/financing">
                              Financing
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link csoon" to="/shop">
                              Shop
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link" to="/dealers">
                              Dealers
                            </Link>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <div className="col-lg-4">
                      <div className="top-bar__inner row justify-content-end align-items-center">
                        <div className="dropdown">
                          <button
                            className="btn btn-secondary dropdown-toggle btn-sm"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            style={{ fontSize: "14px" }}
                          >
                            عربي
                          </button>
                          <div
                            className="dropdown-menu"
                            aria-labelledby="dropdownMenuButton"
                          >
                            <Link className="dropdown-item" href="/">
                              English
                            </Link>
                          </div>
                        </div>
                        <div className="border1 mr-4">&nbsp;</div>
                        <div>
                          <li className="dropdown country-select open">
                            <Link
                              to="/"
                              data-toggle="dropdown"
                              aria-label="Countries"
                              aria-expanded="true"
                            >
                              <span className="flag-sa f-sa" />
                              <i className="mr-5" aria-hidden="true">
                                <span
                                  className="pl-1 mt-3"
                                  style={{
                                    fontSize: "13px",
                                    fontFamily: '"Montserrat"',
                                    fontWeight: 700,
                                    fontStyle: "normal"
                                  }}
                                >
                                  KSA
                                </span>
                              </i>
                            </Link>
                          </li>
                          <li className="dropdown country-select open d-none">
                            <Link
                              to="/"
                              data-toggle="dropdown"
                              aria-label="Countries"
                              aria-expanded="true"
                            >
                              <span className="flag-uae f-uae" />
                              <i className="mr-5" aria-hidden="true">
                                <span
                                  className="pl-1 mt-3"
                                  style={{
                                    fontSize: "13px",
                                    fontFamily: '"Montserrat"',
                                    fontWeight: 700,
                                    fontStyle: "normal"
                                  }}
                                >
                                  UAE
                                </span>
                              </i>
                            </Link>
                          </li>
                        </div>
                        <div className>
                          <Link
                            role="button"
                            className="btn btn-outline-light btn-sm btn-s"
                            to="/LoginSignup"
                          >
                            Sign In / Register
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>

        <div className="appFrame"> 
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route path="/Listing" component={Listing}></Route>
            <Route path="/promotions" component={Promotions}></Route>
            <Route path="/financing" component={Financing}></Route>
            <Route path="/shop" component={Shop}></Route>
            <Route path="/dealers" component={Dealers}></Route>
            <Route path="/Advancesearch" component={Advancesearch}></Route>
            <Route path="/FAQ" component={FAQ}></Route>
            <Route path="/PrivacyPolicy" component={PrivacyPolicy}></Route>
            <Route path="/TermsOfUse" component={TermsOfUse}></Route>
            <Route path="/ContactUs" component={ContactUs}></Route>
            <Route path="/Advertise" component={Advertise}></Route>
            <Route path="/Careers" component={Careers}></Route>
            <Route path="/OurPartners" component={OurPartners}></Route>
            <Route path="/PostAd" component={PostAd}></Route>
            <Route exact path="/Newsone" component={Newsone}></Route>
            <Route exact path="/Newstwo" component={Newstwo}></Route>
            <Route exact path="/Newsthree" component={Newsthree}></Route>
            <Route path="/LoginSignup" component={LoginSignup}></Route>
            <Route
              exact
              path="/Forgotpassword"
              component={Forgotpassword}
            ></Route>
            <Route
              exact
              path="/Changepassword"
              component={Changepassword}
            ></Route>
            <Route path="/OTPRegindv" component={OTPRegindv}></Route>
            <Route exact path="/RegisterIndv" component={RegisterIndv}></Route>
          </Switch>
        </div>

        <footer className="footer">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-sm-5">
                <div className="footer-section footer-section_info">
                  <div className="footer-logo mb-3">
                    <Link className="footer-logo__link" to="/">
                      <img className="img-responsive" src={logo1} alt="Logo" />
                    </Link>
                  </div>
                  <div className="footer-info">
                    “Cararabiya” is an online virtual marketplace for car
                    shoppers and sellers. Cararabiya offers complete online and
                    seamless car buying experience.
                  </div>
                  <div className="footer-contacts">
                    <div className="footer-contacts__item">
                      <i className="ic icon-envelope"></i>
                      <a href="mailto:support@domain.com">
                        support@cararabiya.com
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-3">
                <div className="row">
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">Our Company</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <Link href="/">About Us</Link>
                        </li>
                        <li>
                          <Link to="/OurPartners">Our partners</Link>
                        </li>
                        <li>
                          <Link to="/Careers">Careers</Link>
                        </li>
                        <li>
                          <Link to="/PrivacyPolicy">Privacy Policy</Link>
                        </li>
                        <li>
                          <Link to="/TermsOfUse">Terms &amp; Conditions</Link>
                        </li>
                        <li>
                          <Link to="/Advertise">Advertise with Us</Link>
                        </li>
                      </ul>
                    </section>
                  </div>
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">Useful Links</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <a href="/#_NewsReviews">News &amp; Reviews</a>
                        </li>
                        <li>
                          <a href="/#_testimonial">Testimonials</a>
                        </li>
                        <li>
                          <Link to="/Listing/Listing?Type_ID_C=1">
                            Latest Cars
                          </Link>
                        </li>
                        <li>
                          <a
                            href="#"
                            data-toggle="modal"
                            data-target="#ReviewModal"
                          >
                            Leave Us a Review
                          </a>
                        </li>
                      </ul>
                    </section>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-3">
                <div className="row">
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">CarArabiya</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <Link to="/Listing/Listing?Type_ID_C=2">
                            Used Cars
                          </Link>
                        </li>
                        <li>
                          <Link to="/Listing/Listing?Type_ID_C=1">
                            New Cars
                          </Link>
                        </li>
                        <li>
                          <Link to="/Listing/Listing?Type_ID_C=3">
                            Certified Cars
                          </Link>
                        </li>
                        <li>
                          <Link to="/Promotions">Offers &amp; Promotions</Link>
                        </li>
                        <li>
                          <Link to="/Finance">Financing</Link>
                        </li>
                        <li>
                          <a href="#" onclick="alert('Page is  Coming Soon!');">
                            Shop
                          </a>
                        </li>
                      </ul>
                    </section>
                  </div>
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">Support</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <Link to="/FAQ">FAQ</Link>
                        </li>
                        <li>
                          <a onclick="alert('Page is  Coming Soon!');">
                            Live Chat
                          </a>
                        </li>
                        <li>
                          <Link to="/ContactUs">Contact Us</Link>
                        </li>
                      </ul>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-xs-2"></div>
              <div className="col-lg-4 col-xs-8">
                <div className="footer-social-bar">
                  <div className="text-center">
                    <ul className="footer-soc list-unstyled">
                      <li className="footer-soc__item">
                        <a
                          className="footer-soc__link"
                          href="#"
                          onclick="alert('Page is  Coming Soon!');"
                        >
                          <i className="ic fab fa-twitter fa-2x"></i>
                        </a>
                      </li>
                      <li className="footer-soc__item">
                        <a
                          className="footer-soc__link"
                          href="#"
                          onclick="alert('Page is  Coming Soon!');"
                        >
                          <i className="ic fab fa-facebook fa-2x"></i>
                        </a>
                      </li>
                      <li className="footer-soc__item">
                        <a
                          className="footer-soc__link"
                          href="#"
                          onclick="alert('Page is  Coming Soon!');"
                        >
                          <i className="ic fab fa-instagram fa-2x"></i>
                        </a>
                      </li>
                      <li className="footer-soc__item">
                        <a
                          className="footer-soc__link"
                          href="#"
                          onclick="alert('Page is  Coming Soon!');"
                        >
                          <i className="ic fab fa-linkedin fa-2x"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-xs-2"></div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 px-0">
              <div className="footer-copyright footer_bottom_copyright">
                Copyrights (c) 2020 CarArabiya. All rights reserved.
              </div>
            </div>
          </div>
        </footer>
      </div>
    </Router>
  );
}

export default App;
