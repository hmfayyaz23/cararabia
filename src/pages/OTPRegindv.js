import React, { Component } from 'react';
import './OTP.css';

export default class OTPRegindv extends Component {
    render() {
        return (
            <div>
                 <div className="logmod__wrapper">
                 <div className="logmod__container">
                 <ul className="row">
                <div className="col-12">
                    
                        <div className="row">
                    <div className="col-6"><span className="float-signin">Sign In</span></div> 
                    <div className="col-6"><span className="signreg">Register</span></div> 
                    </div>
                    
                    </div>
               
              </ul>
                 <div className="logmod__tab lgm-3 show">
                <div className="logmod__alter">
                  
                  <div className="logmod__alter-container">
                    <div className="logmod__heading mt-5">
                      <span className="logmod__heading-subtitle">Verify Email Address
                      </span></div>
                  </div>
                </div>
                <span className="logmod__heading-otp text-center"> OTP has been sent to your e-mail,<br />please enter below</span>
                <div className="d-flex justify-content-center mb-1 mt-3">
                  <form className="otp mb-3" autoComplete="off" validate>
                    <fieldset>
                      <input onInput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" maxLength={1} />
                      <input onInput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" maxLength={1} />
                      <input onInput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" maxLength={1} />
                      <input onInput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" maxLength={1} />
                      <input onInput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" maxLength={1} />
                      <input className="mr-0" onInput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" maxLength={1} />
                    </fieldset>
                  </form>
                </div>
                <div className="d-flex justify-content-center mb-2 mt-4"><button type="button" className="btn" style={{border: 'none', padding: '10px 80px', backgroundColor: '#ff5500', color: 'white'}} onclick="window.open('congrats-register.html','_self'); ">Submit</button>
                </div>
                <div className="class-viewall d-flex justify-content-center mb-5"><a href="#">Resend OTP</a></div>
              </div> 
            </div>
            </div>
            </div>
        )
    }
}
