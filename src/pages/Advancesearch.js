import React, { Component } from 'react';
import './Advancesearch.css';

class Advancesearch extends Component {
    render() {
        return (
            <div>

<div>
  <div className="modal fade" id="exampleModalLong" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLongTitle" style={{display: 'none'}} aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLongTitle">Make</h5>
        </div>
        <div className="modal-body" style={{height: '500px', overflowY: 'scroll'}}>
          <div className="accordion" id="accordion-Make-1">
            <div className="card">
              <div className="card-header d-flex justify-content-between" id="headingOne-M">
                <input className="form-check-input"  defaultValue={1} id="Mak_1" name="Make" title="Acura" type="checkbox" />
                <button id="AccorMake_1" className="accordion-trigger collapsed" type="button" data-toggle="collapse" data-target="#collapse_M_1" aria-expanded="false" aria-controls="collapseOne" style={{textDecoration: 'none'}}>Acura <i className="fas fa-chevron-down"></i>
                </button></div>
              <div className="collapse" id="collapse_M_1" aria-labelledby="headingOne" data-target="#accordion-Make-1" aria-expanded="false" style={{}}>
                <div className="card-body ml-3">
                  <div className="accordion" id="accordion-N-Model-1">
                    <div className="card">
                      <div className="card-header d-flex justify-content-between" id="headingOne-N">
                        <input className="form-check-input" defaultValue={1} id="Mod_1" title="CL" name="Model" type="checkbox" />
                        <button id="AccorModel_1" className="accordion-trigger" type="button" data-toggle="collapse" data-target="#collapse_N_1" aria-expanded="true" aria-controls="collapseOne" style={{textDecoration: 'none'}}>
                          CL
                        </button>
                      </div>
                      <div className="collapse in show" id="collapse_N_1" aria-labelledby="headingOne" data-target="#accordion-N-Model-1" aria-expanded="true" style={{}}>
                        <div className="card-body ml-3">
                       
                        </div>
                      </div>
                    </div>
                  </div>
                  
              
                           
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary btn-sm">Reset</button>
          <button className="btn btn-primary btn-sm" style={{backgroundColor: '#ff5500'}} id="Email_btn" name="modal-myvar" data-dismiss="modal">
            Done
          </button>
        </div>
      </div>
    </div>
  </div>
  {/* modal city */}
  <div className="modal fade" id="exampleModalCity" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCity" style={{display: 'none'}} aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalCity">Select City</h5>
        </div>
        <div className="modal-body">
          <div className="modal-body checkbox-list" style={{height: '500px', overflowY: 'scroll'}}>
            <ul id="make_list_popular" name="make_list" className="list-unstyled inline list-links clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6292} title="Al Qunfudhah" id="Ct_6292" name="City" />
                  Al Qunfudhah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6293} title="At Taif" id="Ct_6293" name="City" />
                  At Taif <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6294} title="Qal at Bishah" id="Ct_6294" name="City" />
                  Qal at Bishah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6295} title="Al Hillah" id="Ct_6295" name="City" />
                  Al Hillah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6296} title="As Sulayyil" id="Ct_6296" name="City" />
                  As Sulayyil <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6297} title="Al Wajh" id="Ct_6297" name="City" />
                  Al Wajh <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6298} title="An Nabk" id="Ct_6298" name="City" />
                  An Nabk <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6299} title="Al Kharj" id="Ct_6299" name="City" />
                  Al Kharj <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6300} title="Najran" id="Ct_6300" name="City" />
                  Najran <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6301} title="Al Bahah" id="Ct_6301" name="City" />
                  Al Bahah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6302} title="Medina" id="Ct_6302" name="City" />
                  Medina <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6303} title="Hail" id="Ct_6303" name="City" />
                  Hail <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6304} title="Jizan" id="Ct_6304" name="City" />
                  Jizan <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6305} title="Makkah" id="Ct_6305" name="City" />
                  Makkah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6306} title="Tabuk" id="Ct_6306" name="City" />
                  Tabuk <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6307} title="Arar" id="Ct_6307" name="City" />
                  Arar <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6308} title="Abha" id="Ct_6308" name="City" />
                  Abha <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6309} title="Ad Damman" id="Ct_6309" name="City" />
                  Ad Damman <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6310} title="Buraydah" id="Ct_6310" name="City" />
                  Buraydah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6311} title="Sakakah" id="Ct_6311" name="City" />
                  Sakakah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6312} title="Riyadh" id="Ct_6312" name="City" />
                  Riyadh <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6313} title="Jeddah" id="Ct_6313" name="City" />
                  Jeddah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6314} title="Hafar al Batin" id="Ct_6314" name="City" />
                  Hafar al Batin <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6315} title="Az Zahran" id="Ct_6315" name="City" />
                  Az Zahran <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6316} title="Al Jubayl" id="Ct_6316" name="City" />
                  Al Jubayl <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6317} title="Al-Qatif" id="Ct_6317" name="City" />
                  Al-Qatif <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6318} title="Yanbu al Bahr" id="Ct_6318" name="City" />
                  Yanbu al Bahr <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6319} title="Al Mubarraz" id="Ct_6319" name="City" />
                  Al Mubarraz <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6320} title="Al Hufuf" id="Ct_6320" name="City" />
                  Al Hufuf <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6321} title="Al Quwayiyah" id="Ct_6321" name="City" />
                  Al Quwayiyah <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6322} title="Rafha" id="Ct_6322" name="City" />
                  Rafha <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6323} title="Dawmat al Jandal" id="Ct_6323" name="City" />
                  Dawmat al Jandal <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary btn-sm">Reset</button>
          <button className="btn btn-primary btn-sm" style={{backgroundColor: '#ff5500'}} id="Email_btn" name="modal-myvar" data-dismiss="modal">
            Done
          </button>
        </div>
      </div>
    </div>
  </div>
  {/* modal trans */}
  <div className="modal fade" id="exampleModalTran" tabIndex={-1} role="dialog" aria-labelledby="exampleModalTran" aria-hidden="true" style={{display: 'none'}} data-backdrop="static" data-keyboard="false">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalTran">Select Transmission</h5>
        </div>
        <div className="modal-body">
          <div className="modal-body checkbox-list">
            <ul id="make_list_popular" name="make_list" className="list-unstyled   clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={1} title="Manual" id="Ct_1" name="City" />
                  Manual <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
            <ul id="make_list_popular" name="make_list" className="list-unstyled   clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={2} title="Fully Automatic" id="Ct_2" name="City" />
                  Fully Automatic <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
            <ul id="make_list_popular" name="make_list" className="list-unstyled   clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={3} title="CVT (Continuous Variable Transmission)" id="Ct_3" name="City" />
                  CVT (Continuous Variable Transmission) <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
            <ul id="make_list_popular" name="make_list" className="list-unstyled   clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={4} title="Semi Automatic" id="Ct_4" name="City" />
                  Semi Automatic <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
            <ul id="make_list_popular" name="make_list" className="list-unstyled   clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={5} title="TipTronic Gearbox" id="Ct_5" name="City" />
                  TipTronic Gearbox <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
            <ul id="make_list_popular" name="make_list" className="list-unstyled   clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6} title="DSG (Direct Shift Gearbox)" id="Ct_6" name="City" />
                  DSG (Direct Shift Gearbox) <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary btn-sm">Reset</button>
          <button className="btn btn-primary btn-sm" style={{backgroundColor: '#ff5500'}} id="Email_btn" name="modal-myvar" data-dismiss="modal">
            Send
          </button>
        </div>
      </div>
    </div>
  </div>
  {/* modal Bodytype */}
  <div className="modal fade" id="exampleModalBodytype" tabIndex={-1} role="dialog" aria-labelledby="exampleModalBodytype" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalBodytype">Select Body Type</h5>
        </div>
        <div className="modal-body">
          <div className="modal-body checkbox-list">
            <ul id="make_list_popular" name="make_list" className="list-unstyled inline list-links clearfix">
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={19} title="Campervan" id="Ct_19" name="City" />
                  Campervan <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={1} title="Convertible" id="Ct_1" name="City" />
                  Convertible <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={3} title="Coupe" id="Ct_3" name="City" />
                  Coupe <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={4} title="Crossover" id="Ct_4" name="City" />
                  Crossover <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={5} title="Fastback" id="Ct_5" name="City" />
                  Fastback <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={6} title="Hatchback" id="Ct_6" name="City" />
                  Hatchback <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={15} title="Jeep" id="Ct_15" name="City" />
                  Jeep <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={7} title="Liftback" id="Ct_7" name="City" />
                  Liftback <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={16} title="Luxuary SUV" id="Ct_16" name="City" />
                  Luxuary SUV <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={18} title="Mini" id="Ct_18" name="City" />
                  Mini <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={8} title="Minibus" id="Ct_8" name="City" />
                  Minibus <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={9} title="Minivan" id="Ct_9" name="City" />
                  Minivan <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={2} title="Pickup" id="Ct_2" name="City" />
                  Pickup <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={10} title="Roadster" id="Ct_10" name="City" />
                  Roadster <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={11} title="Sedan" id="Ct_11" name="City" />
                  Sedan <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={17} title="Sports Car" id="Ct_17" name="City" />
                  Sports Car <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={14} title="SUV" id="Ct_14" name="City" />
                  SUV <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={12} title="Van" id="Ct_12" name="City" />
                  Van <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
              <li>
                <label className="checkbox">
                  <input type="checkbox" defaultValue={13} title="Wagon" id="Ct_13" name="City" />
                  Wagon <span className="badge badge-gray-light pull-right" />
                </label>
              </li>
            </ul>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary btn-sm">Reset</button>
          <button className="btn btn-primary btn-sm" style={{backgroundColor: '#ff5500'}} id="Email_btn" name="modal-myvar" data-dismiss="modal">
            Send
          </button>
        </div>
      </div>
    </div>
  </div>
</div>


<div className="l-main-content-inner" style={{backgroundColor: '#e3e3e3', paddingBottom: '50px'}}>
  <div className="container-fluid" style={{backgroundColor: '#646464', padding: '60px 50px 40px 50px'}}>
    <div className="col-12 d-flex justify-content-center">
      <span style={{fontSize: '30px', fontWeight: 800, color: 'white'}}>Advance Search
      </span>
    </div>
  </div>
  <div className="container bg-shadow-advance rounded-1 mt-5 pb-5" style={{backgroundColor: 'white'}}>
    <form action className="form">
      {/* basic tab */}
      <div className="row pt-5">
        <div className="col-lg-6">
          <div id="forms" className="form-group">
            <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
              <div className="col-4">
                <label htmlFor className="control-label">Make</label>
              </div>
              <div className="col-8">
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={1} title="Acura" id="Mak_1" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Acura">Acura</label>
                </div>                                                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={2} title="Alfa Romeo" id="Mak_2" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Alfa Romeo">Alfa Romeo</label>
                </div>                                                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={3} title="Aptera" id="Mak_3" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Aptera">Aptera</label>
                </div>                                                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={4} title="Aston Martin" id="Mak_4" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Aston Martin">Aston Martin</label>
                </div>
                <div className="savesearch">
                  <a href="#" data-toggle="modal" data-target="#exampleModalLong">more choices</a>
                </div>
              </div>
            </div>
          </div>
          <div className="b-filter-slider ui-filter-slider mt-0" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px', marginBottom: '15px'}}>
            <div className="b-filter-slider__title ml-3 mb-5 mt-2">Price</div>
            <div className="b-filter-slider ui-filter-slider" style={{marginTop: '10px'}}>
              <div className="b-filter-slider__main px-5">
                <div id="filterPrice" />
                <div className="b-filter__row row" style={{marginTop: '0px'}}>
                  <div className="b-filter__item col-md-6 col-lg-12 col-xl-6">
                    <input className="quick-select" id="input-with-keypress-0" style={{font: '500 13px/23px Montserrat'}} />
                  </div>
                  <div className="b-filter__item col-md-6 col-lg-12 col-xl-6">
                    <input className="quick-select" id="input-with-keypress-1" style={{font: '500 13px/23px Montserrat'}} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="forms" className="form-group">
            <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
              <div className="col-4">
                <label htmlFor className="control-label">Fuel/Engine Type</label>
              </div>
              <div className="col-8 pt-1">
                <div className="label-group mb-3">
                  <input className="forms__radio hidden" id="ST_1" name="SellerType" title="Individual" defaultValue={1} type="radio" />
                  <label className="forms__label forms__label-radio forms__label-radio-2 mr-3 pl-4 py-0" htmlFor="ST_1">Petrol</label>
                  <input className="forms__radio hidden" id="ST_2" name="SellerType" title="Dealer" defaultValue={2} type="radio" />
                  <label className="forms__label forms__label-radio forms__label-radio-2 mr-3 pl-4 py-0" htmlFor="ST_2">Diesel</label>
                  <input className="forms__radio hidden" id="ST_3" name="SellerType" title="Bank" defaultValue={3} type="radio" />
                  <label className="forms__label forms__label-radio forms__label-radio-2 mr-3 pl-4 py-0" htmlFor="ST_3">Hybrid</label>
                  <input className="forms__radio hidden" id="ST_6" name="SellerType" title="Insurer" defaultValue={6} type="radio" />
                  <label className="forms__label forms__label-radio forms__label-radio-2 mr-3 pl-4 py-0" htmlFor="ST_6">Electric</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6">
          <div id="forms" className="form-group">
            <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
              <div className="col-4">
                <label htmlFor className="control-label">City</label>
              </div>
              <div className="col-8">
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={6292} title="Al Qunfudhah" id="City_6292" name="City" type="checkbox" />
                  <label className="form-check-label" htmlFor="Al Qunfudhah">Al Qunfudhah</label>
                </div>
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={6293} title="At Taif" id="City_6293" name="City" type="checkbox" />
                  <label className="form-check-label" htmlFor="At Taif">At Taif</label>
                </div>
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={6294} title="Qal at Bishah" id="City_6294" name="City" type="checkbox" />
                  <label className="form-check-label" htmlFor="Qal at Bishah">Qal at Bishah</label>
                </div>
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={6295} title="Al Hillah" id="City_6295" name="City" type="checkbox" />
                  <label className="form-check-label" htmlFor="Al Hillah">Al Hillah</label>
                </div>
                <div className="savesearch">
                  <a href="#" data-toggle="modal" data-target="#exampleModalCity">more choices</a>
                </div>
              </div>
            </div>
          </div>
          <div id="forms" className="form-group">
            <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
              <div className="col-4">
                <label htmlFor className="control-label">Condition</label>
              </div>
              <div className="col-8 pt-1">
                <div className="label-group mb-3">
                  <input className="forms__radio hidden" id="forms__radio-1" type="radio" name="radio-group-3" defaultValue={1} />
                  <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="forms__radio-1" style={{fontSize: '0.9em'}}><span>New</span></label>
                  <input className="forms__radio hidden" id="forms__radio-2" type="radio" name="radio-group-3" defaultValue={2} />
                  <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="forms__radio-2" style={{fontSize: '0.9em'}}><span>Used</span></label>
                </div>
              </div>
            </div>
          </div>
          <div id="forms" className="form-group">
            <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
              <div className="col-4">
                <label htmlFor className="control-label">Body Type</label>
              </div>
              <div className="col-8 pt-1">
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={19} title="Campervan" id="Mak_19" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Campervan">Campervan</label>
                </div>
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={1} title="Convertible" id="Mak_1" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Convertible">Convertible</label>
                </div>
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={3} title="Coupe" id="Mak_3" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Coupe">Coupe</label>
                </div>
                <div className="form-check form-check-inline col-12">
                  <input className="form-check-input" defaultValue={4} title="Crossover" id="Mak_4" name="Make" type="checkbox" />
                  <label className="form-check-label" htmlFor="Crossover">Crossover</label>
                </div>
                <div className="savesearch">
                  <a href="#" data-toggle="modal" data-target="#exampleModalBodytype">more choices</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    <section>
      {/* ============================================================================================================ */}
      <div className="panel-group">
        <div className="panel panel-default">
          {/* more options */}
          <div id="collapse1" className="panel-collapse collapse show" style={{}}>
            <form action className="form">
              {/* basic tab */}
              <div className="row mt-4">
                <div className="col-lg-6">
                  <div id="forms" className="form-group">
                    <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                      <div className="col-4">
                        <label htmlFor className="control-label">Seller Type</label>
                      </div>
                      <div className="col-8 pt-1">
                        <div className="label-group mb-3">
                          <input className="forms__radio hidden" id="forms__radio-1" type="radio" name="radio-group-3" defaultValue={1} />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="forms__radio-1" style={{fontSize: '0.9em'}}><span>Individual</span></label>
                          <input className="forms__radio hidden" id="forms__radio-2" type="radio" name="radio-group-3" defaultValue={2} />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="forms__radio-2" style={{fontSize: '0.9em'}}><span>Dealer</span></label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="b-filter-slider ui-filter-slider mt-3" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">Version Year</div>
                    <div className="b-filter-slider__main px-5">
                      <div id="filterMeter_A" className="noUi-target noUi-ltr noUi-horizontal"><div className="noUi-base"><div className="noUi-connects"><div className="noUi-connect" style={{transform: 'translate(4%, 0px) scale(0.76, 1)'}} /></div><div className="noUi-origin" style={{transform: 'translate(-96%, 0px)', zIndex: 5}}><div className="noUi-handle noUi-handle-lower" data-handle={0} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={0.0} aria-valuemax={400000.0} aria-valuenow={20000.0} aria-valuetext={20000} /></div><div className="noUi-origin" style={{transform: 'translate(-20%, 0px)', zIndex: 4}}><div className="noUi-handle noUi-handle-upper" data-handle={1} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={20000.0} aria-valuemax={500000.0} aria-valuenow={400000.0} aria-valuetext={400000} /></div></div></div>
                      <div className="b-filter__row row">
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 pl-5">
                          <input className="quick-select" id="VerYear_Min_A" />
                        </div>
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 ml-5 pl-5">
                          <input className="quick-select" id="VerYear_Max_A" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="b-filter-slider ui-filter-slider mt-3" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">No. of Doors</div>
                    <div className="b-filter-slider__main px-5">
                      <div id="filterMeter_A" className="noUi-target noUi-ltr noUi-horizontal"><div className="noUi-base"><div className="noUi-connects"><div className="noUi-connect" style={{transform: 'translate(4%, 0px) scale(0.76, 1)'}} /></div><div className="noUi-origin" style={{transform: 'translate(-96%, 0px)', zIndex: 5}}><div className="noUi-handle noUi-handle-lower" data-handle={0} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={0.0} aria-valuemax={400000.0} aria-valuenow={20000.0} aria-valuetext={20000} /></div><div className="noUi-origin" style={{transform: 'translate(-20%, 0px)', zIndex: 4}}><div className="noUi-handle noUi-handle-upper" data-handle={1} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={20000.0} aria-valuemax={500000.0} aria-valuenow={400000.0} aria-valuetext={400000} /></div></div></div>
                      <div className="b-filter__row row">
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 pl-5">
                          <input className="quick-select" id="Door_Min_A" />
                        </div>
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 ml-5 pl-5">
                          <input className="quick-select" id="Door_Max_A" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="forms" className="form-group">
                    <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                      <div className="col-4">
                        <label htmlFor className="control-label">Exterior Color</label>
                      </div>
                      <div className="col-8">
                        <select className="custom-select-adpost">
                          <option selected>Select</option>
                          <option value={1}>White</option>
                          <option value={2}>Amber</option>
                          <option value={3}>Apricot</option>
                          <option value={4}>Azure</option>
                          <option value={5}>Beige</option>
                          <option value={6}>Black</option>
                          <option value={7}>Blue</option>
                          <option value={8}>Blue-Green</option>
                          <option value={9}>Blue-Violet</option>
                        
                          
                        </select>
                      </div>
                    </div>
                  </div>
                  <div id="forms" className="form-group"> {/* Tranmission start */}
                    <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                      <div className="col-4">
                        <label htmlFor className="control-label">Transmission</label>
                      </div>
                      <div className="col-8">
                        <div className="form-check form-check-inline col-12">
                          <input className="form-check-input" defaultValue={1} title="Manual" id="Trans_1" name="Transmission" type="checkbox" />
                          <label className="form-check-label" htmlFor="Manual">Manual</label>
                        </div>
                        <div className="form-check form-check-inline col-12">
                          <input className="form-check-input" defaultValue={2} title="Fully Automatic" id="Trans_2" name="Transmission" type="checkbox" />
                          <label className="form-check-label" htmlFor="Fully Automatic">Fully Automatic</label>
                        </div>
                        <div className="form-check form-check-inline col-12">
                          <input className="form-check-input" defaultValue={3} title="CVT (Continuous Variable Transmission)" id="Trans_3" name="Transmission" type="checkbox" />
                          <label className="form-check-label" htmlFor="CVT (Continuous Variable Transmission)">CVT (Continuous Variable Transmission)</label>
                        </div>
                        <div className="form-check form-check-inline col-12">
                          <input className="form-check-input" defaultValue={4} title="Semi Automatic" id="Trans_4" name="Transmission" type="checkbox" />
                          <label className="form-check-label" htmlFor="Semi Automatic">Semi Automatic</label>
                        </div>
                        <div className="savesearch">
                          <a href="#" data-toggle="modal" data-target="#exampleModalTran">more choices</a>
                        </div>
                      </div>
                    </div>
                  </div>  
                  <div id="forms" className="form-group"> {/* Tranmission start */}
                    <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                      <div className="col-4">
                        <label htmlFor className="control-label">Images</label>
                      </div>
                      <div className="col-8">
                        <div className="form-check form-check-inline col-12">
                          <input className="form-check-input" defaultValue={1} title="Ads without images" id="Imag_1" name="images" type="checkbox" />
                          <label className="form-check-label" htmlFor="Ads without images">Ads without images</label>
                        </div>
                        <div className="form-check form-check-inline col-12">
                          <input className="form-check-input" defaultValue={2} title="Ads with images" id="Imag_2" name="images" type="checkbox" />
                          <label className="form-check-label" htmlFor="Ads with images">Ads with images</label>
                        </div>
                      </div>
                    </div>
                  </div> 
                </div>
                <div className="col-lg-6">
                  <div id="forms" className="form-group">
                    <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                      <div className="col-4">
                        <label htmlFor className="control-label">No. of Owner(s)</label>
                      </div>
                      <div className="col-8 pt-1">
                        <div className="label-group mb-3">
                          <input className="forms__radio hidden" id="forms__radio-1" type="radio" name="radio-group-3" defaultValue={1} />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="forms__radio-1" style={{fontSize: '0.9em'}}><span>1</span></label>
                          <input className="forms__radio hidden" id="forms__radio-2" type="radio" name="radio-group-3" defaultValue={2} />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="forms__radio-2" style={{fontSize: '0.9em'}}><span>2</span></label>
                          <input className="forms__radio hidden" id="forms__radio-3" type="radio" name="radio-group-3" defaultValue="3+" />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="forms__radio-3" style={{fontSize: '0.9em'}}><span>3+</span></label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="forms" className="form-group">
                    <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                      <div className="col-4">
                        <label htmlFor className="control-label">Drive Side</label>
                      </div>
                      <div className="col-8 pt-1">
                        <div className="label-group mb-3">
                          <input className="forms__radio hidden" id="DS_LHD" name="DriveSide" defaultValue="LHD" title="LHD" type="radio" />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="DS_LHD" style={{fontSize: '0.9em'}}><span>Left Hand Drive</span></label>
                          <input className="forms__radio hidden" id="DS_RHD" name="DriveSide" title="RHD" defaultValue="RHD" type="radio" />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="DS_RHD" style={{fontSize: '0.9em'}}><span>Right Hand Drive</span></label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="forms" className="form-group">
                    <div className="row" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                      <div className="col-4">
                        <label htmlFor className="control-label">Assembly</label>
                      </div>
                      <div className="col-8 pt-1">
                        <div className="label-group mb-3">
                          <input className="forms__radio hidden" id="As_Imp" name="Assembly" defaultValue="Imp" title="Imp" type="radio" />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="As_Imp" style={{fontSize: '0.9em'}}><span>Imported</span></label>
                          <input className="forms__radio hidden" id="As_Loc" name="Assembly" title="Loc" defaultValue="Loc" type="radio" />
                          <label className="forms__label forms__label-radio forms__label-radio-2 mr-3" htmlFor="As_Loc" style={{fontSize: '0.9em'}}><span>Local</span></label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="b-filter-slider ui-filter-slider mt-3" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">Meter Reading</div>
                    <div className="b-filter-slider__main px-5">
                      <div id="filterMeter_A" className="noUi-target noUi-ltr noUi-horizontal"><div className="noUi-base"><div className="noUi-connects"><div className="noUi-connect" style={{transform: 'translate(4%, 0px) scale(0.76, 1)'}} /></div><div className="noUi-origin" style={{transform: 'translate(-96%, 0px)', zIndex: 5}}><div className="noUi-handle noUi-handle-lower" data-handle={0} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={0.0} aria-valuemax={400000.0} aria-valuenow={20000.0} aria-valuetext={20000} /></div><div className="noUi-origin" style={{transform: 'translate(-20%, 0px)', zIndex: 4}}><div className="noUi-handle noUi-handle-upper" data-handle={1} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={20000.0} aria-valuemax={500000.0} aria-valuenow={400000.0} aria-valuetext={400000} /></div></div></div>
                      <div className="b-filter__row row">
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 pl-5">
                          <input className="quick-select" id="Meter_Min_A" />
                        </div>
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 ml-5 pl-5">
                          <input className="quick-select" id="Meter_Max_A" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="b-filter-slider ui-filter-slider mt-3" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">Mileage</div>
                    <div className="b-filter-slider__main px-5">
                      <div id="filterMeter_A" className="noUi-target noUi-ltr noUi-horizontal"><div className="noUi-base"><div className="noUi-connects"><div className="noUi-connect" style={{transform: 'translate(4%, 0px) scale(0.76, 1)'}} /></div><div className="noUi-origin" style={{transform: 'translate(-96%, 0px)', zIndex: 5}}><div className="noUi-handle noUi-handle-lower" data-handle={0} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={0.0} aria-valuemax={400000.0} aria-valuenow={20000.0} aria-valuetext={20000} /></div><div className="noUi-origin" style={{transform: 'translate(-20%, 0px)', zIndex: 4}}><div className="noUi-handle noUi-handle-upper" data-handle={1} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={20000.0} aria-valuemax={500000.0} aria-valuenow={400000.0} aria-valuetext={400000} /></div></div></div>
                      <div className="b-filter__row row">
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 pl-5">
                          <input className="quick-select" id="Mileage_Min_A" />
                        </div>
                        <div className="b-filter__item col-md-5 col-lg-12 col-xl-5 ml-5 pl-5">
                          <input className="quick-select" id="Mileage_Max_A" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="b-filter-slider ui-filter-slider mt-0" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px', marginBottom: '15px'}}>
                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">Engine Size</div>
                    <div className="b-filter-slider__main px-5">
                      <div id="filterEngin_A" className="noUi-target noUi-ltr noUi-horizontal"><div className="noUi-base"><div className="noUi-connects"><div className="noUi-connect" style={{transform: 'translate(12%, 0px) scale(0.68, 1)'}} /></div><div className="noUi-origin" style={{transform: 'translate(-88%, 0px)', zIndex: 5}}><div className="noUi-handle noUi-handle-lower" data-handle={0} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={0.0} aria-valuemax={4000.0} aria-valuenow={600.0} aria-valuetext={600} /></div><div className="noUi-origin" style={{transform: 'translate(-20%, 0px)', zIndex: 4}}><div className="noUi-handle noUi-handle-upper" data-handle={1} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={600.0} aria-valuemax={5000.0} aria-valuenow={4000.0} aria-valuetext={4000} /></div></div></div>
                      <div className="b-filter__row row">
                        <div className="b-filter__item col-md-5 col-lg-11 col-xl-5 pl-5">
                          <input className="quick-select" id="Engin_Min_Size_A" />
                        </div>
                        <div className="b-filter__item col-md-5 col-lg-11 col-xl-5 ml-4 pl-5">
                          <input className="quick-select" id="Engin_Max_Size_A" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <div className="b-filter-slider ui-filter-slider mt-3" style={{border: '1px solid #ff5500', borderRadius: '8px', padding: '10px'}}>
                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">Picture</div>
                    <div className="b-filter-slider__main px-5">
                      <div id="filterMeter_A" className="noUi-target noUi-ltr noUi-horizontal"><div className="noUi-base"><div className="noUi-connects"><div className="noUi-connect" style={{transform: 'translate(4%, 0px) scale(0.76, 1)'}} /></div><div className="noUi-origin" style={{transform: 'translate(-96%, 0px)', zIndex: 5}}><div className="noUi-handle noUi-handle-lower" data-handle={0} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={0.0} aria-valuemax={400000.0} aria-valuenow={20000.0} aria-valuetext={20000} /></div><div className="noUi-origin" style={{transform: 'translate(-20%, 0px)', zIndex: 4}}><div className="noUi-handle noUi-handle-upper" data-handle={1} tabIndex={0} role="slider" aria-orientation="horizontal" aria-valuemin={20000.0} aria-valuemax={500000.0} aria-valuenow={400000.0} aria-valuetext={400000} /></div></div></div>
                      <div className="b-filter__row row">
                        <input className="form-check-input" onChange={this.handlePicChange} id="picm-1" name="pic" type="checkbox" />
                        <label className="form-check-label" htmlFor="pic">Include Picture</label>
                      </div>
                    </div>
                  </div> */}
                </div>
              </div>
            </form>
          </div>
          <div className="panel-heading" style={{backgroundColor: 'white'}}>
            <h4 className="panel-title mt-4 pl-4 pb-5 optiontoggle more">
              <a data-toggle="collapse" href="#collapse1" style={{font: '600 16px/1 Montserrat', color: '#ff5500'}}><span>More options</span></a><i className="fa fa-caret-down pl-2" style={{color: '#ff5500', fontSize: '14px'}} />
              <a className="btn next-step" style={{position: 'absolute', right: '20px', padding: '10px 80px', backgroundColor: '#ff5500', color: 'white', transform: 'translateY(-10px)'}}>Search</a>
            </h4>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>



             
  </div>
      
        );
    }
}

export default Advancesearch;