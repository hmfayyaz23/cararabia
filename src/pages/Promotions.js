import React, { Component } from 'react';
import Promoadvertisment from '../components/promotions/Promoadvertisment';
import Promolatestads from '../components/promotions/Promolatestads';
import Promolist from '../components/promotions/Promolist';
import Promosearch from '../components/promotions/Promosearch';


class Promotions extends Component {
    render() {
        return (
            <div>
                <div className="l-main-content-inner">
  <div className="modal fade" id="redeemModal" tabIndex={-1} role="dialog" aria-labelledby="redeemModalLabel" aria-hidden="true">
    <div className="modal-dialog modal-dialog-centered modal-md" role="document">
      <div className="modal-content">
        <div className="modal-body">
          <div className="mt-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4><strong>Send Message to Dealer</strong></h4>
            </div>
            <div>
              <input type="text" className="form-control" placeholder="Enter" />
            </div>
            <div className="mt-3">
              <textarea name id cols={30} rows={5} className="form-control" placeholder="Send message" defaultValue={""} />
            </div>
            <div className="d-flex justify-content-center mb-2 mt-3"><button type="button" className="btn" style={{border: 'none', padding: '10px 80px', backgroundColor: '#ff5500', color: 'white'}} data-dismiss="modal">SEND</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="container mt-5">
    <div className="row">
      <div className="col-lg-3">
        <Promosearch/>
        <Promoadvertisment/>
      </div>
      <div className="col-lg-9">
       <Promolatestads/>
        <Promolist/>
      </div>
    </div>
  </div>
</div>

                
            </div>
        );
    }
}

export default Promotions;