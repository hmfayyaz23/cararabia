import React, { Component } from 'react'

export default class OurPartners extends Component {
    render() {
        return (
            <div>
                <div className="l-main-content-inner" style={{minHeight: '780px', paddingTop: '49px'}}>
  <div className="container-fluid" style={{backgroundColor: '#646464', padding: '60px 50px 40px 50px'}}>
    <div className="col-12 d-flex justify-content-center">
      <span style={{fontSize: '30px', fontWeight: 800, color: 'white'}}>Our Partners</span>
    </div>
  </div>
  <div className="container-fluid" style={{backgroundColor: 'white', padding: '80px 50px 80px 50px'}}>
    <div className="container">
      <div className="col-12 d-flex ">
        <span>
          <p style={{fontSize: '18px', fontWeight: 500, textAlign: 'left', color: '#464646', lineHeight: '1.4', padding: '0 20px'}}>
            Cararabiya is an online virtual marketplace for car shoppers and sellers. Cararabiya offers complete online and seamless car buying experience. We have partners across the country from different sectors. 
          </p>
        </span>
      </div>
    </div>
  </div>
  <div className="container-fluid" style={{backgroundColor: '#e9e9e9'}}>
    <div className="container pt-5">
      <div className="col-12"><h4 className="ui-title-ad" style={{color: '#464646'}}>BANKING SECTOR PARTNERS</h4></div>
      <div className="b-gallery js-slider px-5" data-slick="{&quot;slidesToShow&quot;: 4, &quot;arrows&quot;: false, &quot;autoplay&quot;: true,  &quot;slidesToScroll&quot;: 2, &quot;rows&quot;: 1, &quot;responsive&quot;: [{&quot;breakpoint&quot;: 1400, &quot;settings&quot;: {&quot;slidesToShow&quot;: 3, &quot;slidesToScroll&quot;: 3}}, {&quot;breakpoint&quot;: 768, &quot;settings&quot;: {&quot;slidesToShow&quot;: 2, &quot;slidesToScroll&quot;: 1}}]}">
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/adcb.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/adib.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/ajmanbank.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/dib.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/EIB.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/RAKbank.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/adcb.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/adib.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/ajmanbank.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/dib.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/EIB.png" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/RAKbank.png" alt="foto" /></a></div>
      </div>
      <br /><br />
    </div>
  </div> 
  <div className="container-fluid" style={{backgroundColor: 'white'}}>
    <div className="container pt-5">
      <div className="col-12 mb-4"><h4 className="ui-title-ad">INDUSTRY PARTERS</h4></div>
      <div className="b-gallery js-slider px-5" data-slick="{&quot;slidesToShow&quot;: 4, &quot;arrows&quot;: false, &quot;autoplay&quot;: true,  &quot;slidesToScroll&quot;: 2, &quot;rows&quot;: 1, &quot;responsive&quot;: [{&quot;breakpoint&quot;: 1400, &quot;settings&quot;: {&quot;slidesToShow&quot;: 3, &quot;slidesToScroll&quot;: 3}}, {&quot;breakpoint&quot;: 768, &quot;settings&quot;: {&quot;slidesToShow&quot;: 2, &quot;slidesToScroll&quot;: 1}}]}">
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/souqarabiya.jpg" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/f1.jpg" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/tijarat.jpg" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/KBC.jpg" alt="foto" /></a></div>&lt;
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/f1.jpg" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/tijarat.jpg" alt="foto" /></a></div>
        <div className="b-gallery__item text-center"><a href="bank-credit-policy.html"><img className="img-fluid" src="./assets/media/KBC.jpg" alt="foto" /></a></div>
      </div>
      <br /><br />
    </div>
  </div>    
</div>

            </div>
        )
    }
}
