import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class PlaceAnAd extends Component {
    render() {
        return (
            <div>
               
                <div id="feedback">
                    <Link to="/PostAd">Place an Ad</Link>
                </div>
                <div id="saved">
                    <Link to="/"><i className="fas fa-heart" /></Link>
                </div>            
           
            </div>
        )
    }
}
