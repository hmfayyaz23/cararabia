import React, { Component } from 'react';
import Dealerdetailadsbyuser from '../components/dealerdetail/Dealerdetailadsbyuser'
import Dealerdetailadvertisement from '../components/dealerdetail/Dealerdetailadvertisement'
import Dealerdetailbranches from '../components/dealerdetail/Dealerdetailbranches'
import Dealerdetailmain from '../components/dealerdetail/Dealerdetailmain'
import Dealerdetailpromo from '../components/dealerdetail/Dealerdetailpromo'

class Dealerdetail extends Component {
    constructor(props)
    {
        super(props);
        this.state = { data: this.props.match.params};

        //this.setState({
        //    data: this.props.match.params
        //  });
        
    }
    render() {
        //const dealerid= this.props.match.params;
        //console.log(dealerid)
        return (
            <div>
                <Dealerdetailmain id={this.state.data.dealerid} ></Dealerdetailmain>
                <Dealerdetailadsbyuser id={this.state.data.dealerid}></Dealerdetailadsbyuser>
                <Dealerdetailbranches id={this.state.data.dealerid}></Dealerdetailbranches>
                <Dealerdetailpromo id={this.state.data.dealerid}></Dealerdetailpromo>
                <Dealerdetailadvertisement id={this.state.data.dealerid}></Dealerdetailadvertisement>
            </div>
        );
    }
}

export default Dealerdetail;