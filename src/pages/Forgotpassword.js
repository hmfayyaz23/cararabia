import React, { Component } from 'react';
import './Forgotpassword.css'

export default class Forgotpassword extends Component {
    render() {
        return (
            <div>
                <div className="logmod__wrapper">
                    <div className="logmod__container">
                        <div className="logmod__tab lgm-4">
                            <div className="logmod__alter">
                                <div className="logmod__alter-container">
                                    <div className="logmod__heading">
                                        <span className="logmod__heading-password mt-4 mb-4"><strong>Forgot Password</strong>
                                        </span></div>
                                </div>
                                <ul className="logmod__tabs">
                                    <li data-tabtar="lgm-4">
                                        <div className="d-flex justify-content-center mb-3">
                                            <input type="email" className="form-control address" id="address" placeholder="Email" />
                                        </div>
                                        <div className="d-flex justify-content-center mb-5 mt-5">
                                            <button type="submit" role="button" className="btn btn-block btn-dark-blue">RESET PASSWORD</button>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
