import React, { Component } from 'react';
import './LoginSignup.css';
import {Link} from 'react-router-dom';

export default class LoginSignup extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid" style={{minHeight: '1000px'}}>
  <div className="container">
    <div className="row">
      <div className="col-lg-12">
        <div className="logmod">
          <div className="logmod__wrapper">
            <div className="logmod__container">
              <ul className="logmod__tabs row">
                <li data-tabtar="lgm-2" style={{transform: 'translateX(-45px)'}}><a href="#">Sign In</a></li>
                <li data-tabtar="lgm-1"><a href="#"><span className="current">Register</span></a></li>
              </ul>
              <div className="logmod__tab-wrapper">
                <div className="logmod__tab lgm-1">
                  <div className="mt-5">
                    <div className="row justify-content-center">
                      <ul>
                        <li>
                          <div className="col-6">
                            <div className="bg-shadow py-5 indv-box">
                              <Link to="/RegisterIndv"> <img src="./assets/media/individual.png" width="130px" height="auto" alt="" /></Link>
                              
                              <span className="mt-3" style={{display: 'block', font: '600 18px/1 Montserrat'}}>As Individual</span>
                            </div>
                          </div>
                        </li>
                      </ul>
                      <ul>
                        <li>
                          <div className="col-6">
                            <div className="bg-shadow py-5 dealer-box" onclick="window.open('Registeration-Dealer.html','_self'); ">
                              <img src="assets/media/dealer.png" width="130px" height="auto" alt="" />
                              <span className="mt-3" style={{display: 'block', font: '600 18px/1 Montserrat'}}>As Dealer</span>
                            </div>
                          </div></li>
                      </ul>
                    </div>
                  </div>
                </div>  
                
              </div>
              <div className="logmod__tab lgm-2">
                <div className="logmod__alter">
                  <div className="logmod__alter-container">
                    <div className="logmod__heading mt-3">
                      <span className="logmod__heading-subtitle">Sign in with your favourite social network
                      </span></div>
                    <div className="social-login">
                      <div className="social-login">
                        <button className="btn facebook-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-facebook-f" /> </span> </button>
                        <button className="btn twitter-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-twitter" /></span> </button>
                        <button className="btn linkedin-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-linkedin" /></span> </button>
                        <button className="btn instagram-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-instagram" /></span> </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="logmod__heading">
                  <div className="OR"> <span>OR</span></div>
                  <hr style={{margin: '0px'}} />
                </div> 
                <div>
                  <span className="logmod__heading-subtitlee">Sign in with Email</span>
                </div>
                <div className="logmod__form">
                  <form acceptCharset="utf-8" action="#" className="simform">
                    <div className="sminputs">
                      <div className="input full">
                        <label className="string optional" htmlFor="user-name">Email*</label>
                        <input className="string optional" maxLength={255} id="user-email" placeholder="Email" type="email" size={50} required/>
                      </div>
                    </div>
                    <div className="sminputs">
                      <div className="input full">
                        <label className="string optional" htmlFor="user-pw">Password *</label>
                        <input className="string optional" maxLength={255} id="user-pw" placeholder="Password" type="password" size={50} required/>
                        <span className="hide-password"><i className="fas fa-eye" /></span>
                      </div>
                    </div>
                    <div className="simform__actions">
                      <div><input className="sumbit btn btn-block mt-3" name="commit" type="submit" defaultValue="Sign In" /></div>
                    </div>
                    <div><span className="forget mb-4"><Link className="special" role="link" to="/Forgotpassword">Forgot your password?</Link></span></div>
                    {/* <div><span className="forget mb-4"><Link className="special" role="link" to="/Changepassword">Change your password?</Link></span></div>  */}
                  </form>
                </div> 
              </div>
             
            </div>
          </div>
        </div> 
      </div>  {/* logmod__wrapper */}
    </div>{/*  class="logmod"> */}
  </div>{/* class="col-lg-12"> */}
</div> {/* class="row"> */}
{/* class="container"> */}

            </div>
        )
    }
}
