import React, { Component } from 'react';
import Homeslider from '../components/home/Homeslider';
import Homesearch from '../components/home/Homesearch';
import Homebanner from '../components/home/Homebanner';
import Homepromo from '../components/home/Homepromo';
import Homerecentads from '../components/home/Homerecentads';
import Homeadvertise from '../components/home/Homeadvertise';
import Homecounter from '../components/home/Homecounter';
import Homenews from '../components/home/Homenews';
import Hometestimonials from '../components/home/Hometestimonials';
import Homebuysell from '../components/home/Homebuysell';

class Home extends Component {
    render() {
        return (
            <div>
               <Homeslider></Homeslider>
               <Homesearch></Homesearch>
               <Homebanner></Homebanner>
               <Homepromo></Homepromo>
               <Homerecentads></Homerecentads>
               <Homeadvertise></Homeadvertise>
               <Homecounter></Homecounter>
               <Homenews></Homenews>
               <Hometestimonials></Hometestimonials>
               <Homebuysell></Homebuysell> 
            </div>
        );
    }
}

export default Home;