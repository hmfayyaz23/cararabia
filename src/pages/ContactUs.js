import React, { Component } from 'react'

export default class ContactUs extends Component {
    render() {
        return (
            <div>
                <div className="l-main-content-inner" style={{minHeight: '780px', paddingTop: '49px'}}>
  <div className="container-fluid" style={{backgroundColor: '#646464', padding: '60px 50px 40px 50px'}}>
    <div className="col-12 d-flex justify-content-center">
      <span style={{fontSize: '30px', fontWeight: 800, color: 'white'}}>Contact Us</span>
    </div>
  </div>
  <div className="container">
    <div className="row mt-5">
      <div className="col-md-8">
        <form action="/post" method="post">
          <input className="form-control" name="name" placeholder="Name..." style={{border: '1px solid #ff550099'}} /><br />
          <input className="form-control" name="phone" placeholder="Phone..." style={{border: '1px solid #ff550099'}} /><br />
          <input className="form-control" name="email" placeholder="E-mail..." style={{border: '1px solid #ff550099'}} /><br />
          <textarea className="form-control" name="text" placeholder="How can we help you?" style={{height: '150px', border: '1px solid #ff550099'}} defaultValue={""} /><br />
          <input style={{backgroundColor: '#ff5500'}} className="btn btn-primary" type="submit" defaultValue="Send" /><br /><br />
        </form>
      </div>
      <div className="col-md-4">
        <b>Customer service:</b> <br />
        Phone: +91 129 209 291<br />
        E-mail: <a href="mailto:support@cararabiya.com" style={{color: '#ff5500'}}>support@cararabiya.com</a><br />
        <br /><br />
        <b>Headquarter:</b><br />
        Company Inc, <br />
        Sheikh Abdullah road 201<br />
        55001 Jeddah, KSA<br />
        Phone: +91 145 000 101<br />
        <a href="mailto:hq@cararabiya.com" style={{color: '#ff5500'}}>hq@cararabiya.com</a><br />
        <br />
        <b>Pakistan:</b><br />
        Company CA Ltd, <br />
        25/F.168 Queen<br />
        Ghazi District, Karachi<br />
        Phone: +92 129 209 291<br />
        <a href="mailto:info@cararabiya.com" style={{color: '#ff5500'}}>info@cararabiya.com</a><br />
      </div>
    </div>
  </div>
</div>

            </div>
        )
    }
}
