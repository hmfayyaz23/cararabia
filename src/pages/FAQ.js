import React, { Component } from 'react'

export default class FAQ extends Component {
    render() {
        return (
            <div>
                <div className="l-main-content-inner" style={{minHeight: '780px', paddingTop: '49px'}}>
  <div className="container-fluid" style={{backgroundColor: '#646464', padding: '60px 50px 40px 50px'}}>
    <div className="col-12 d-flex justify-content-center">
      <span style={{fontSize: '30px', fontWeight: 800, color: 'white'}}>Frequently Asked Questions</span>
    </div>
  </div>
  <div className="container">
    <div className="col-12 mt-5">
      <section>
        {/*Accordion wrapper*/}
        <div className="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
          {/* Accordion card */}
          <div className="card" style={{borderBottom: '1px solid #99999978'}}>
            {/* Card header */}
            <div className="card-header" role="tab" id="headingOne1" style={{padding: '1rem 1.5rem'}}>
              <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1" className="collapsed" style={{color: '#ff5500'}}>
                <h5 className="mb-0">
                  Q.1: What is CarArabiya? <i className="fas fa-angle-down rotate-icon" />
                </h5>
              </a>
            </div>
            {/* Card body */}
            <div id="collapseOne1" className="collapse" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx" style={{}}>
              <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                <p>Cararabiya is an online virtual marketplace for car shoppers and sellers. Cararabiya
                  offers complete and seamless online car buying experience.
                </p>
              </div>
            </div>
          </div>
          {/* Accordion card */}
          {/* Accordion card */}
          <div className="card faq" style={{borderBottom: '1px solid #99999978'}}>
            {/* Card header */}
            <div className="card-header" role="tab" id="headingTwo2" style={{padding: '1rem 1.5rem'}}>
              <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2" style={{color: '#ff5500'}}>
                <h5 className="mb-0">
                  Q.2: How do I register for CarArabiya? <i className="fas fa-angle-down rotate-icon" />
                </h5>
              </a>
            </div>
            {/* Card body */}
            <div id="collapseTwo2" className="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx" style={{}}>
              <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                {/*  ● Searching for cars on our website is easier and faster than ever. To begin, just go to our home page and decide how you would like to search. You can use the buttons ,you can specify several criteria at once, or you can search by keywords that you type in. */}
                <ul>
                  <p>Registration is a quick and simple process :</p>
                  <li>This can be done by clicking on the sign in/register icon.</li>
                  <li>You can register via your social media profiles or by using your valid email address and
                    phone number. You will receive a notification confirming your registration.
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* Accordion card */}
          {/* Accordion card */}
          <div className="card" style={{borderBottom: '1px solid #99999978'}}>
            {/* Card header */}
            <div className="card-header" role="tab" id="headingThree3" style={{padding: '1rem 1.5rem'}}>
              <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3" style={{color: '#ff5500'}}>
                <h5 className="mb-0">
                  Q.3: How do I sign in? <i className="fas fa-angle-down rotate-icon" />
                </h5>
              </a>
            </div>
            {/* Card body */}
            <div id="collapseThree3" className="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
              <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                <ul>
                  <li>Sign in can be done by clicking on the Sign in/register icon.</li>
                  <li>If you have registered via your social media profiles then please use the relevant icons to
                    sign in.
                  </li>
                  <li>Or if you have used your email address to register you will have to use your
                    email address to sign in.
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* Accordion card */}
          <div className="card" style={{borderBottom: '1px solid #99999978'}}>
            {/* Card header */}
            <div className="card-header" role="tab" id="headingFour4" style={{padding: '1rem 1.5rem'}}>
              <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4" aria-expanded="false" aria-controls="collapseFour4" style={{color: '#ff5500'}}>
                <h5 className="mb-0">
                  Q.4: Why am I unable to sign in? <i className="fas fa-angle-down rotate-icon" />
                </h5>
              </a>
            </div>
            {/* Card body */}
            <div id="collapseFour4" className="collapse" role="tabpanel" aria-labelledby="headingFour4" data-parent="#accordionEx">
              <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                <ul>
                  <li>Please check your sign in details such as email address, password and make sure
                    that the correct information is being inserted. Kindly save your login details in a safe and
                    secure place.
                  </li>
                  <li>Please check if you have completed the registration process by entering OTP received via email. If you haven’t please complete the registration process. You can also contact our support team for further
                    assistance.
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* Accordion card */}
          {/* Accordion card */}
          <div className="card" style={{borderBottom: '1px solid #99999978'}}>
            {/* Card header */}
            <div className="card-header" role="tab" id="headingFive5" style={{padding: '1rem 1.5rem'}}>
              <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFive5" aria-expanded="false" aria-controls="collapseFive5" style={{color: '#ff5500'}}>
                <h5 className="mb-0">
                  Q.5: What if I forgot my password? <i className="fas fa-angle-down rotate-icon" />
                </h5>
              </a>
            </div>
            {/* Card body */}
            <div id="collapseFive5" className="collapse" role="tabpanel" aria-labelledby="headingFive5" data-parent="#accordionEx">
              <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                <ul>
                  <li>To retrieve forgotten password please click the link forgot password on sign in  page. If you have used your email address to register you will be asked to enter the same
                    email address.</li>
                  <li>A link to reset password will be sent on the registered email address. Or if you have registered via social media profiles please check if you still have access to these social
                    media profiles and try signing in again.
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* Accordion card */}
          {/* Accordion card */}
          <div className="card" style={{borderBottom: '1px solid #99999978'}}>
            {/* Card header */}
            <div className="card-header" role="tab" id="headingSix6" style={{padding: '1rem 1.5rem'}}>
              <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSix6" aria-expanded="false" aria-controls="collapseSix6" style={{color: '#ff5500'}}>
                <h5 className="mb-0">
                  Q.6: what you do....Why do you do?  <i className="fas fa-angle-down rotate-icon" />
                </h5>
              </a>
            </div>
            {/* Card body */}
            <div id="collapseSix6" className="collapse" role="tabpanel" aria-labelledby="headingSix6" data-parent="#accordionEx">
              <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                <p>
                  CarArabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.
                  CarArabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.
                  CarArabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.
                </p>
              </div>
            </div>
          </div>
          {/* Accordion card */}
          {/* Accordion card */}
          <div className="card" style={{borderBottom: '1px solid #99999978'}}>
            {/* Card header */}
            <div className="card-header" role="tab" id="headingSeven7" style={{padding: '1rem 1.5rem'}}>
              <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSeven7" aria-expanded="false" aria-controls="collapseSeven7" style={{color: '#ff5500'}}>
                <h5 className="mb-0">
                  Q.7: How do I access CarArabiya?  <i className="fas fa-angle-down rotate-icon" />
                </h5>
              </a>
            </div>
            {/* Card body */}
            <div id="collapseSeven7" className="collapse" role="tabpanel" aria-labelledby="headingSeven7" data-parent="#accordionEx">
              <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px', listStyleType: 'square'}}>
                <ul>
                  <li style={{listStyleType: 'square'}}>CarArabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.</li>
                  <li style={{listStyleType: 'square'}}>CarArabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.</li>
                  <li style={{listStyleType: 'square'}}>CarArabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.</li>
                </ul>
              </div>
            </div>
          </div>
          {/* Accordion card */}
        </div>
        {/* Accordion wrapper */}
      </section>
    </div>
  </div>
</div>



            </div>
        )
    }
}
