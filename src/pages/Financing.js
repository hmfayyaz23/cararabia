import React, { Component } from 'react';
import Financingtop from '../components/financing/Financingtop';
import Financingmainright from '../components/financing/Financingmainright';
import Financingmainleft from '../components/financing/Financingmainleft';

class Financing extends Component {
    render() {
        return (
            <div>
                <div className="l-main-content-inner">
                    <Financingtop />
                    <br /><br />
                    <div className="container-fluid" style={{ backgroundColor: '#e8e8e8' }}>
                        <div className="container pt-5 pb-5">
                            <div className="row">
                                <div className="col-lg-6">
                                    <Financingmainleft />
                                </div>
                                <div className="col-lg-6">
                                    <Financingmainright />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Financing;