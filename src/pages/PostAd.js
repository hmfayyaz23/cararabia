import React, { Component } from 'react';
import './PostAd.css';

export default class PostAd extends Component {
    render() {
        return (
            <div>
                <div className="l-main-content-inner" style={{minHeight: '1000px'}}>
  <div className="section-area">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div id="pricingplans">
            <div className="modal fade" id="PackageModal" tabIndex={-1} role="dialog" aria-labelledby="PackageModalLabel" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-body">
                    <section className="pricing-area pb-50" id="pricing">
                      <div className="container">
                        <div className="row">
                          <div className="col-xl-8 mx-auto text-center">
                            <div className="section-title">
                              <h2>select package</h2>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-xl-4">
                            <div className="single-price bg-shadow">
                              <div className="price-title">
                                <h4>BASIC</h4>
                              </div>
                              <div className="price-tag">
                                <h2>FREE</h2>
                              </div>
                              <div className="price-item">
                                <ul>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Upto 5 photos</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />30 Days of Run time</li>
                                  <li><i className="fas fa-times pr-1" style={{color: 'red'}} />Ad View Report</li>
                                  <li><i className="fas fa-times pr-1" style={{color: 'red'}} />Featured Listing</li>
                                  <li><i className="fas fa-times pr-1" style={{color: 'red'}} />Allow 5 Ads to posting</li>
                                </ul>
                              </div>
                              <a href="#" className="box-btn" data-dismiss="modal">SELECT PLAN</a>
                            </div>
                          </div>
                          <div className="col-xl-4">
                            <div className="single-price">
                              <div className="price-title">
                                <h4>PRO</h4>
                              </div>
                              <div className="price-tag">
                                <h2><span style={{fontSize: '20px', fontWeight: 600}}>SAR</span> 50 <span>/ AD</span></h2>
                              </div>
                              <div className="price-item">
                                <ul>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Upto 10 photos</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />60 Days of Run time</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Ad View Report</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Featured Listing</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Allow 10 Ads to posting</li>
                                </ul>
                              </div>
                              <a href="#" className="box-btn" data-dismiss="modal" data-toggle="modal" data-target="#PaymentModal" onclick>SELECT PLAN</a>
                            </div>
                          </div>
                          <div className="col-xl-4">
                            <div className="single-price">
                              <div className="price-title">
                                <h4>PREMIUM</h4>
                              </div>
                              <div className="price-tag">
                                <h2><span style={{fontSize: '20px', fontWeight: 600}}>SAR</span> 100 <span>/ AD</span></h2>
                              </div>
                              <div className="price-item">
                                <ul>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Upto 15 photos</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />150 Days of Run time</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Ad View Report</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Featured Listing</li>
                                  <li><i className="fas fa-check pr-1" style={{color: '#47c77e'}} />Allow 12 Ads to posting</li>
                                </ul>
                              </div>
                              <a href="#" className="box-btn" data-toggle="modal" data-target="#PaymentModal">SELECT PLAN</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="paymentgateway">
            <div className="modal fade" id="PaymentModal" tabIndex={-1} role="dialog" aria-labelledby="PaymentModalLabel" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-body">
                    <article className="card">
                      <div className="card-body p-5">
                        <p> 
                          <span style={{font: '800 24px/1 Montserrat', color: '#464646'}}>Payment Information</span>      
                          <img style={{float: 'right'}} src="assets/media/payment-icons.png" /> 
                        </p>
                        <form role="form" className="mt-3">
                          <div className="form-group">
                            <label htmlFor="username">Full name (on the card)</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-user" /></span>
                              </div>
                              <input type="text" className="form-control" style={{color: 'black'}} name="username" placeholder required />
                            </div> {/* input-group.// */}
                          </div> {/* form-group.// */}
                          <div className="form-group">
                            <label htmlFor="cardNumber">Card number</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-credit-card" /></span>
                              </div>
                              <input type="text" className="form-control" style={{color: 'black'}} name="cardNumber" placeholder />
                            </div> {/* input-group.// */}
                          </div> {/* form-group.// */}
                          <div className="row">
                            <div className="col-sm-8 pl-0">
                              <div className="form-group">
                                <label style={{float: 'initial'}}><span className="hidden-xs">Expiration</span> </label>
                                <div className="form-inline">
                                  <select className="form-control" style={{width: '45%', color: 'black'}}>
                                    <option>MM</option>
                                    <option>January</option>
                                    <option>February</option>
                                    <option>March</option>
                                    <option>April</option>
                                    <option>May</option>
                                    <option>June</option>
                                    <option>July</option>
                                    <option>August</option>
                                    <option>September</option>
                                    <option>October</option>
                                    <option>November</option>
                                    <option>December</option>
                                  </select>
                                  <span style={{width: '10%', textAlign: 'center'}}> / </span>
                                  <select className="form-control" style={{width: '45%', color: 'black'}}>
                                    <option>YY</option>
                                    <option>2022</option>
                                    <option>2023</option>
                                    <option>2024</option>
                                    <option>2025</option>
                                    <option>2026</option>
                                    <option>2027</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="form-group">
                                <label data-toggle="tooltip" title data-original-title="3 digits code on back side of the card">CVV <i className="fa fa-question-circle" /></label>
                                <input className="form-control" required type="text" />
                              </div> {/* form-group.// */}
                            </div>
                          </div> {/* row.// */}
                          <button className="subscribe btn btn-primary btn-block cararabiya-color mt-2" type="button" data-dismiss="modal"> Confirm</button>
                        </form>
                      </div> {/* card-body.// */}
                    </article> {/* card.// */}                             
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="modal fade" id="successModal" tabIndex={-1} role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <div className="mt-5">
                    <div className="d-flex justify-content-center">
                      <img src="assets/media/success.png" alt="" />
                    </div>
                    <div className="d-flex justify-content-center mt-3">
                      <h4><strong>Your Ad has been Posted Successfully!</strong></h4>
                    </div>
                    <div className="d-flex justify-content-center mt-3">
                      <p>It will be visible to buyers in few minutes</p>
                    </div>
                    <div className="d-flex justify-content-center mt-3" style={{color: '#ff5500'}}>
                      <h4>Your Ad will be valid till 10 October 2019</h4>
                    </div>
                    <div className="d-flex justify-content-center mt-5">
                      <p><b>Share your ad with your friends</b></p>
                    </div>
                    <div className="d-flex justify-content-center mt-3">
                      <div className="social-login">
                        <button className="btn facebook-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-facebook-f" /> </span> </button>
                        <button className="btn twitter-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-twitter" /></span> </button>
                        <button className="btn linkedin-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-linkedin" /></span> </button>
                        <button className="btn instagram-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-instagram" /></span> </button>
                      </div>
                    </div>
                    <div className="d-flex justify-content-center mb-2 mt-5"><button type="button" className="btn" style={{border: 'none', padding: '10px 80px', backgroundColor: '#ff5500', color: 'white'}} onclick="window.open('inventory-list-new.html','_self'); ">View your listing</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="b-find">
            <ul className="b-find-nav nav nav-tabs justify-content-between" id="findTab" role="tablist">
              <li className="b-find-nav__item nav-item t-bi">
                <a className="b-find-nav__link nav-link active" id="tab-allCar" data-toggle="tab" href="#content-basic" role="tab" aria-controls="content-allCar" aria-selected="true">Basic Info</a>
              </li>
              <li className="b-find-nav__item nav-item t-ai"><a className="b-find-nav__link nav-link " id="tab-allCar" data-toggle="tab" href="#content-additional" role="tab" aria-controls="content-allCar" aria-selected="true" style={{padding: '17px 42px'}}>Additional Info</a></li>
              <li className="b-find-nav__item nav-item t-ui"><a className="b-find-nav__link nav-link" id="tab-newCars" data-toggle="tab" href="#content-images" role="tab" aria-controls="content-newCars" aria-selected="false" style={{padding: '17px 42px'}}>Upload Images</a></li>
              <li className="b-find-nav__item nav-item t-pr"><a className="b-find-nav__link nav-link" id="tab-newCars" data-toggle="tab" href="#content-preview" role="tab" aria-controls="content-shop" aria-selected="false">Preview</a></li>
            </ul>
            <div className="b-find-content tab-content" id="findTabContent" style={{borderColor: 'white'}}>
              <div className="tab-pane fade show active" id="content-basic">
                <form action className="form-horizontal"> {/* basic tab */}
                  <div className="row mt-5">
                    <div className="col-lg-6">
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">City *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select City</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>        
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Area *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select Area</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Sale Price *</label>
                          </div>
                          <div className="col-8">
                            <input type="text" className="form-control" placeholder="Enter" />
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Make *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Model *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Version *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Year *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Exterior Color *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Mileage (Km/Ltr) *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Car Type *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Interior Color *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Meter Reading(km)*</label>
                          </div>
                          <div className="col-8">
                            <input type="text" className="form-control" placeholder="Enter" />
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label"># of Owners *</label>
                          </div>
                          <div className="col-8">
                            <select className="custom-select-adpost">
                              <option selected>Select</option>
                              <option value={1}>Option 1</option>
                              <option value={2}>Option 2</option>
                              <option value={3}>Option 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Fuel Type *</label>
                          </div>
                          <div className="col-8">
                            <input type="text" className="form-control" placeholder="Enter" />
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Engine Capacity (cc) *</label>
                          </div>
                          <div className="col-8">
                            <input type="text" className="form-control" placeholder="Enter " />  
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor className="control-label">Ad Description *</label>
                          </div>
                          <div className="col-8">
                            <textarea name id cols={30} rows={5} className="form-control" placeholder="Enter description" defaultValue={""} />
                          </div>
                        </div>
                      </div>
                    </div>                      
                  </div> 
                </form>                
                <div className="mt-3">  
                  <a className="btn next-step" style={{position: 'absolute', zIndex: 151, right: 0, padding: '10px 80px', backgroundColor: '#ff5500', color: 'white'}}>Continue</a>
                </div>
              </div>
              <div className="tab-pane fade" id="content-additional"> {/* additonal info */}
                <div className="row mt-5">
                  <div className="col-lg-6">
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Drive Side</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter " />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Assembly</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter " />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Registration City</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter " />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Chassis Number / VIN</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Transmission</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Engine Type</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Engine Number</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">No. of Cylinders</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">No. of Doors</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">No. of Gears</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">No. of Seats</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />  
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label htmlFor className="control-label">Trim</label>
                        </div>
                        <div className="col-8">
                          <input type="text" className="form-control" placeholder="Enter" />    
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <div className="row">
                    <div className="mt-4 col-12">
                      <h4 style={{fontFamily: '"Montserrat"', fontSize: '18px', fontWeight: 600}}>Features</h4>
                      <hr />
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>   
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>                                 
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>                                 
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-check">
                        <input className="form-check-input mt-2" type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                        <label className="form-check-label">Audio Controls on Steering Wheel
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="mt-5"> 
                    <div>
                      <a className="btn previous-step">Previous</a>
                      <a className="btn next-step">Continue</a>
                    </div>
                  </div>
                </div> 
              </div>
              <div className="tab-pane fade" id="content-images">
                <div className="mt-5">
                  <div className="col-12 upload-image-section">
                    <form action className="dropzone" id="product-image-drpzone">
                      <div className="px-3 mt-5 d-flex justify-content-center">
                        <label htmlFor="file-upload" className="custom-file-upload-orange">
                          Upload Images</label>
                        <input id="file-upload" type="file" />
                      </div>
                      {/* <div className="dz-message">
                        <span><strong>Drag &amp; Drop upto 5 photos, or
                            <a className="btn-choose-file btn-link" id="btn-upload">browse.</a></strong>
                        </span>
                      </div> */}
                    </form>
                  </div>
                  {/* <div class="col-12 d-flex justify-content-center mt-4">
                    <div id="previews" class="dropzone-previews">
                      <div class="col-sm-2 dz-preview drp-image">
                        <img src="assets/no-image.png">
                      </div>
                      <div class="col-sm-2 dz-preview drp-image">
                        <img src="assets/no-image.png">
                      </div>
                      <div class="col-sm-2 dz-preview drp-image">
                        <img src="assets/no-image.png">
                      </div>
                       <div class="col-sm-2 dz-preview drp-image">
                        <img src="assets/no-image.png">
                      </div>
                       <div class="col-sm-2 dz-preview drp-image">
                        <img src="assets/no-image.png">
                      </div>
                    </div>
             </div> */}
                </div>
                <div className="mt-5"> 
                  <div>
                    <a className="btn previous-step">Previous</a>
                    <a className="btn next-step">Continue</a>
                  </div>
                </div>
              </div>
              <div className="tab-pane fade" id="content-preview">
                <div className="row mt-3">
                  <div className="col-lg-8">
                    <div className="result-box">
                      <img id="result" style={{width: '100%'}} />
                    </div>
                    <div className="roww">
                      <div className="thumbs">
                        <img src="assets/media/1.jpg" width={100} height={100} onclick="myFunction(this);" />
                      </div>
                      <div className="thumbs">
                        <img src="assets/media/2.jpg" width={100} height={100} onclick="myFunction(this);" />
                      </div>
                      <div className="thumbs">
                        <img src="assets/media/3.jpg" width={100} height={100} onclick="myFunction(this);" />
                      </div>
                      <div className="thumbs">
                        <img src="assets/media/4.jpg" width={100} height={100} onclick="myFunction(this);" />
                      </div>
                      <div className="thumbs">
                        <img src="assets/media/5.jpg" width={100} height={100} onclick="myFunction(this);" />
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-6 col-sm-6 mt-3 mb-3">
                      <ul className="ad-detail-icon d-flex">
                        <li><span>YEAR</span><img src="assets/media/icon/Calendar.png" width={30} height={30} alt="" /><strong>2017</strong></li>
                        <li><span>KILOMETERS</span><img src="assets/media/icon/Km.png" width={30} height={30} alt="" /><strong>13,785</strong></li>
                        <li><span>COLOR</span><img src="assets/media/icon/Color.png" width={30} height={30} alt="" /><strong>Red</strong></li>
                        <li><span>DOORS</span><img src="assets/media/icon/Doors.png" width={30} height={30} alt="" /><strong>4 door</strong></li>
                        <li><span>TRANSMISSION</span><img src="assets/media/icon/Automatic.png" width={30} height={30} alt="" /><strong>Automatic</strong></li>
                        <li><span>BODY TYPE</span><img src="assets/media/icon/BodyCondition.png" width={30} height={30} alt="" /><strong>Sedan</strong></li>
                      </ul>
                    </div>
                    <br />
                    <h5>Seller Notes</h5>
                    <div className="row mt-3">
                      <div className="col-md-12">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad modi nisi non dolor quibusdam veniam odio commodi sunt, doloremque ex nulla maiores ab soluta dolorem temporibus, libero explicabo corporis repellat.</p>
                      </div>
                    </div>
                    <br />
                    <h5>Basic Details</h5>
                    <div className="row mt-3">
                      <div className="col-md-6">
                        <dl className="b-goods-f__descr row">
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>VERSION YEAR</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>2007</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>BRAND</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>BMW</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>MODEL</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>1 SERIES</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>BODY STYLE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>COUPE</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>EXTERIOR COLOR</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>BLACK F01</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE #</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>123</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>CHASSIS #</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>111</dt>
                          <hr className="col-12 mt-0  px-0" />
                        </dl>
                      </div>
                      <div className="col-md-6">
                        <dl className="b-goods-f__descr row">
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>VEHICLE TYPE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>USED</dt>
                          <hr className="col-12 mt-0 px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>METER READING</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>222 Kms</dt>
                          <hr className="col-12 mt-0 px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE SIZE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>1995 cc</dt>
                          <hr className="col-12 mt-0 px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF CYLINDERS</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>4 No(s).</dt>
                          <hr className="col-12 mt-0 px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>DRIVE SIDE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>LHD</dt>
                          <hr className="col-12 mt-0 px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ASSEMBLY</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>
                            IMPORTED
                          </dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>REGISTERED CITY</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>QAL AT BISHAH</dt>
                          <hr className="col-12 mt-0  px-0" />
                        </dl>
                      </div>
                    </div>
                    <br />
                    <h5>Additional Info</h5>
                    <div className="row mt-3">
                      <div className="col-md-6">
                        <dl className="b-goods-f__descr row">
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>MILEAGE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>5.4 Km/Ltr</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF DOORS</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>2 No(s).</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF SEATS</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>4 Pers</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF GEARS</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>3 No(s).</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF OWNERS</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>3</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>DRIVE TYPE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>RWD</dt>
                          <hr className="col-12 mt-0  px-0" />
                        </dl>
                      </div>
                      <div className="col-md-6">
                        <dl className="b-goods-f__descr row">
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE POWER</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>143 bhp</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE TYPE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>DIESEL</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>FUEL TANK SIZE</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>51 Ltr.</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>INTERIOR COLOR</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>BLACK 668</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>BODY CONDITION</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>0 out of 10</dt>
                          <hr className="col-12 mt-0  px-0" />
                          <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>TRANSMISSION</dd>
                          <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>FULLY AUTOMATIC</dt>
                          <hr className="col-12 mt-0  px-0" />
                        </dl>
                      </div>
                    </div>
                    <br />
                    <h5>Features</h5>
                    <div className="row">
                      <div className="col-12 mt-3">
                        <div className="row px-0">
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Air Conditioning </dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Alloy Rims</dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Automatic AC</dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>ABS</dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Adaptive Lighting</dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>AM/FM Radio </dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Blind Spot Detection Mirror</dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Audio Controls on Steering Wheel</dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Bluetooth System</dd>
                            </dl>
                          </div>
                          <div className="col-6 px-0">
                            <dl className="b-goods-f__descr mb-0">
                              <dd className="b-goods-f__descr-title" style={{fontWeight: 400}}>Cassette Player</dd>
                            </dl>
                          </div>                      
                        </div>      
                      </div>
                    </div>
                  </div>
                  {/* end .b-seller*/}
                  <div className="col-lg-4 px-0"> {/* price section */}
                    <div className style={{backgroundColor: '#ff5500', textAlign: 'center'}}> 
                      <div className="b-seller__title py-3">
                        <div className="b-seller__price">SAR 90,000</div>
                        <span style={{color: 'white', fontSize: '11px', textAlign: 'center'}}>prices are exclusive of VAT</span>
                      </div>
                    </div>
                    <div> {/* buttons */}
                      <div className="b-goods-f__sidebar px-4 py-3" style={{backgroundColor: '#e0e0e0'}}>
                        <div className="b-goods-f__price-group justify-content-center">
                          {/* <span class="b-goods-f__price">
                                 <span class="b-goods-f__price-numb">SAR &nbsp; 50,000</span>
                              </span> */}
                          <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <i style={{position: 'absolute', left: '45px'}} className="fas fa-phone prefix" /><span>Show Phone Number</span></button>
                          <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <i style={{position: 'absolute', left: '45px'}} className="fas fa-envelope prefix " /><span>Show Email</span></button>
                          <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <i style={{position: 'absolute', left: '45px'}} className="far fa-comment-dots prefix" /><span>Chat with seller</span>
                          </button>
                        </div> 
                        <span className="b-goods-f__compare">
                        </span>
                      </div>
                      <div className="b-goods-f__sidebar px-4 py-2" style={{backgroundColor: '#e0e0e0', borderTop: '1px solid #cecece'}}>
                        <div className="b-goods-f__price-group justify-content-center">
                          {/* <span class="b-goods-f__price">
                                 <span class="b-goods-f__price-numb">SAR &nbsp; 50,000</span>
                              </span> */}
                          <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <span>Installment Calculator</span></button>
                          <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <span>Apply for Finance</span></button>
                        </div> 
                        <span className="b-goods-f__compare">
                        </span>
                      </div>
                      <div className="b-goods-f__sidebar px-4 py-2" style={{backgroundColor: '#e0e0e0', borderTop: '1px solid #cecece'}}>
                        <div className="bg-detail-seller">
                          <div className="b-seller__detail pt-3 px-0 py-2" style={{backgroundColor: 'transparent'}}>
                            <div className="b-seller__img"><img className="img-scale" src="./assets/media/content/b-seller/1.jpg" alt="foto" /></div>
                            <div className="b-seller__title">
                              <div className="b-seller__name" style={{fontWeight: 600, fontSize: '16px'}}>Akif Sheikh</div>
                              <div className="b-seller__category">Member Since August 2001</div>
                            </div>
                            <hr />
                            <div className="b-seller__detail row d-flex justify-content-center px-0" style={{backgroundColor: 'transparent'}}>
                              <div className="mx-2">
                                <i style={{color: '#ff5500', fontSize: '30px', paddingRight: '10px', transform: 'translateY(5px)'}} className="fas fa-phone" /><span style={{fontSize: '12px'}}> Phone verified</span></div>
                              <div className="mx-2"><i style={{color: '#ff5500', fontSize: '30px', paddingRight: '10px', transform: 'translateY(5px)'}} className="fas fa-envelope" /><span style={{fontSize: '12px'}}> E-mail verified</span></div>
                            </div>
                          </div>
                          <span className="b-goods-f__compare" style={{padding: '0px'}}>
                          </span>
                        </div>
                      </div>
                      <div className="my-3 pb-2 d-flex justify-content-center" style={{borderBottom: '1px solid #cecece'}}>
                        <div>
                          <a style={{color: 'grey'}} href="#"><span style={{fontSize: '1.1em'}} className="mr-3 pb-2"><i className="far fa-envelope mr-1" /> Email this listing</span></a>
                        </div>
                      </div>
                      <div className="d-flex justify-content-end">
                        <a style={{color: 'grey'}} href="#"><span style={{fontSize: '0.9em'}} className="mr-3 pb-2"><i className="fas fa-flag mr-1" /> Report this Ad</span></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mt-5"> 
                  <div>
                    <a className="btn previous-step" style={{position: 'absolute', right: '510px', padding: '10px 80px', backgroundColor: '#ff5500', color: 'white'}}>Previous</a>
                    <a className="btn previous-step" style={{position: 'absolute', right: '250px', padding: '10px 60px', backgroundColor: '#666', color: 'white'}}>Save as Draft</a>
                    <a className="btn next-step" style={{position: 'absolute', right: 0, padding: '10px 80px', backgroundColor: '#ff5500', color: 'white'}} data-toggle="modal" data-target="#successModal">Post Ad</a>
                  </div>
                </div>
              </div>
              <div className="tab-pane fade" id="content-success"> {/* success tab */}
                <div className="mt-5">
                  <div className="d-flex justify-content-center">
                    <img src="assets/media/success.png" alt="" />
                  </div>
                  <div className="d-flex justify-content-center mt-3">
                    <h4><strong>Your Ad has been Posted Successfully!</strong></h4>
                  </div>
                  <div className="d-flex justify-content-center mt-3">
                    <p>It will be visible to buyers in few minutes</p>
                  </div>
                  <div className="d-flex justify-content-center mt-3" style={{color: '#ff5500'}}>
                    <h4>Your Ad will be valid till 10 October 2019</h4>
                  </div>
                  <div className="d-flex justify-content-center mt-5">
                    <p><b>Share your ad with your friends</b></p>
                  </div>
                  <div className="d-flex justify-content-center mt-3">
                    <div className="social-login">
                      <button className="btn facebook-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-facebook-f" /> </span> </button>
                      <button className="btn twitter-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-twitter" /></span> </button>
                      <button className="btn linkedin-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-linkedin" /></span> </button>
                      <button className="btn instagram-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-instagram" /></span> </button>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center mb-2 mt-5"><button type="button" className="btn" style={{border: 'none', padding: '10px 80px', backgroundColor: '#ff5500', color: 'white'}} onclick="window.open('inventory-list-new.html','_self'); ">View your listing</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

            </div>
        )
    }
}
