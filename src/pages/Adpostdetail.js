import React, { Component } from 'react';
import Adpostdetailmain from '../components/adpostdetail/Adpostdetailmain'
import Adpostdetailsimilar from '../components/adpostdetail/Adpostdetailsimilar'
class Adpostdetail extends Component {

    constructor(props)
    {
        super(props);
        this.state = { 
            data: this.props.match.params            
        };        
    }

    render() {
        return (
            <div>
                <Adpostdetailmain id={this.state.data.adpostid}></Adpostdetailmain>
                <Adpostdetailsimilar></Adpostdetailsimilar>              

            </div>
        );
    }
}

export default Adpostdetail;