import React, { Component } from 'react';

class Newsdetailrecentnews extends Component {
    render() {
        return (
            <div>
                <section className="widget section-sidebar bg-light">
                    <h3 className="widget-title bg-cararabiya">recent posts</h3>
                    <div className="widget-content">
                    <div className="widget-inner">
                        <section className="post-widget clearfix">
                        <div className="post-widget__media"><a onclick="window.location.href='/News/News2';"><img className="img-fluid" src="/Content/assets/media/content/b-posts/80x80/2.jpg" alt="foto" /></a></div>
                        <div className="post-widget__inner">
                            <h2 className="post-widget__title">
                            <a onclick="window.location.href='/News/News2';">
                                Inside the Porsche Taycan Factory: How Porsche Will Build Its Electric Sports Car
                            </a>
                            </h2>
                            <div className="post-widget__date">
                            <time dateTime="2019-10-27 15:20">Dec 20, 2019</time>
                            </div>
                        </div>
                        {/* end .widget-post*/}
                        </section>
                        <section className="post-widget clearfix">
                        <div className="post-widget__media"><a onclick="window.location.href='/News/News3';"><img className="img-fluid" src="/Content/assets/media/content/b-posts/80x80/3.jpg" alt="foto" /></a></div>
                        <div className="post-widget__inner">
                            <h2 className="post-widget__title">
                            <a onclick="window.location.href='/News/News3';">
                                2019 Subaru WRX STI S209 First Test: What Makes You So Special?
                            </a>
                            </h2>
                            <div className="post-widget__date">
                            <time dateTime="2019-10-27 15:20">Dec 20, 2019</time>
                            </div>
                        </div>
                        {/* end .widget-post*/}
                        </section>
                    </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Newsdetailrecentnews;