import React, { Component } from 'react';

class Adpostdetailmain extends Component {
  constructor(props)
    {
        super(props);
        this.state = { 
            adpostid: this.props.id, 
            dataadpost: [],
            dataimage: [],
            dataatt: [],
            phone: '',
            email: '',
            adpostprice: '',
            dpay: '',
            tenure: '',
            irate: '',
            emi:'',
            totalamountto: '',
            totalinterestamount: '', 
            error: null, 
            validationError: '' };

        this.handlephone = this.handlephone.bind(this);
        this.handleemail = this.handleemail.bind(this);
        this.handledpaychange = this.handledpaychange.bind(this);
        this.handletenurechange = this.handletenurechange.bind(this);
        this.handleiratechange = this.handleiratechange.bind(this);
        this.handlepricechange = this.handlepricechange.bind(this);
        this.handlecalcuclick = this.handlecalcuclick.bind(this);
    }
    componentDidMount () {
        const apiUrlAdpost = 'http://cararabiya.ahalfa.com/api/AdPostDetail/AdPostDetail?AdPostID='+ this.state.adpostid;
        const apiUrlAtt = 'http://cararabiya.ahalfa.com/api/AdPostAttribute?AdPostID='+ this.state.adpostid;
        const apiUrlImage = 'http://cararabiya.ahalfa.com/api/adimage?AdpostID='+ this.state.adpostid;

        fetch(apiUrlAdpost)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                dataadpost: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )

          fetch(apiUrlAtt)
          .then(res => res.json())
          .then(
            (result) => {
              if(result.length != null)
              {
                this.setState({
                  dataatt: result
                });
              }
              else
              {
                this.setState({
                  dataatt: [{"AttributeName": "No data found"}]
                });
              }
               
              
            },
            (error) => {
              this.setState({ error });
              
            }
          )

          fetch(apiUrlImage)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                dataimage: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )
          
    }

    handlephone(event) {
      var userid='';
      
      this.state.dataadpost.map(adpost => (
        userid = adpost.UserID
      ))

      

      const apiUrluserphone = 'http://cararabiya.ahalfa.com/api/User/UserMobile?UserID='+ userid;
      
      fetch(apiUrluserphone)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                phone: result
              })
            },
            (error) => {
              this.setState({ error });
            }
          )
          
    }

    handleemail(event) {

      var userid='';
      
      this.state.dataadpost.map(adpost => (
        userid = adpost.UserID
      ))

      

      const apiUrluseremail = 'http://cararabiya.ahalfa.com/api/User/UserEmail?UserID='+ userid;
      
      fetch(apiUrluseremail)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                email: result
              })
            },
            (error) => {
              this.setState({ error });
            }
          )

    }

    handledpaychange(event){
      const newdpay = event.target.value;
    
        this.setState({
          dpay: newdpay
        })
    }
    handletenurechange(event){
      const newtenure = event.target.value;
    
        this.setState({
          tenure: newtenure
        })
    }
    handleiratechange(event){
      const newirate = event.target.value;
    
        this.setState({
          irate: newirate
        })
    }
    handlepricechange(event){
      const newprice = event.target.value;
    
        this.setState({
          adpostprice: newprice
        })
    }
    handlecalcuclick(event){

      

      var txt_ID_Price = event.target.id;
      var txt_ID_DPay = this.state.dpay;
      var txt_ID_Tenure = this.state.tenure;
      var txt_ID_IRate = this.state.irate;

      var Tenure = txt_ID_Tenure * 12;
      var InterestRate = (txt_ID_IRate / 100) / 12;
      var DownPayment = txt_ID_DPay / 100;
      var FinanceAmount = txt_ID_Price * (1 - DownPayment);

      var I = 1 + InterestRate;
      var y = Math.pow(I, Tenure);
      var yNew = y * InterestRate;


      var z = Math.pow((1 + InterestRate), Tenure);
      var zNew = z - 1;
      var xvalue = yNew / zNew;
      var EMI = Math.round(FinanceAmount * xvalue);
      var PrincipleAmount = Math.round((EMI / xvalue) / (1 - DownPayment));
      var TotalAmountto = Math.round(EMI * Tenure);
      var DownpaymentAmount = PrincipleAmount * DownPayment;
      var TotalInterestAmount = Math.round(TotalAmountto * InterestRate * 12);
      
      this.setState({
        emi: EMI,
        totalamountto: TotalAmountto,
        totalinterestamount: TotalInterestAmount
      })
      
      
      
     // $("#ID_Emi").text(EMI);
     // $("#ID_Tot_Amt").text(TotalAmountto);
     // $("#ID_Tot_IRat").text(TotalInterestAmount);

    }



  render() {
    const image1='http://cararabiyaweb.ahalfa.com/Content/assets/media/content/b-seller/1.jpg';
    
    
       return (      
      <div>
        <div className="modal fade bd-example-modal-sm" id="myphone_modal" tabIndex={-1} role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered modal-sm">
            <div className="modal-content">
              <div className="d-flex justify-content-center" style={{padding: '20px', flexDirection: 'column', textAlign: 'center'}}>
                <p style={{font: '600 16px/1 Montserrat', paddingBottom: '10px'}}>User Phone Number</p>
                <span>
                  {this.state.phone}
                </span>
                <span>
                  <button data-dismiss="modal" className="btn btn-primary mt-3" style={{backgroundColor: '#ff5500'}}>
                    OK
                  </button>
                </span>
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade bd-example-modal-sm" id="myemail_modal" tabIndex={-1} role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered modal-sm">
            <div className="modal-content">
              <div className="d-flex justify-content-center" style={{padding: '20px', flexDirection: 'column', textAlign: 'center'}}>
                <p style={{font: '600 16px/1 Montserrat', paddingBottom: '10px'}}>User Email</p>
                <span>
                  {this.state.email}
                </span>
                <span>
                  <button data-dismiss="modal" className="btn btn-primary mt-3" style={{backgroundColor: '#ff5500'}}>
                    OK
                  </button>
                </span>
              </div>
            </div>
          </div>
        </div>

       {this.state.dataadpost.map(adpost => (
                    <div>
                        
                        <div className="col-12 px-0">
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb py-0 ">
                                    <li className="breadcrumb-item"><a>Home</a></li>
                                    <li className="breadcrumb-item" aria-current="page">{adpost.VehicleTypeName} Cars for Sale</li>
                                    <li className="breadcrumb-item" aria-current="page">{adpost.MakeName} </li>
                                    <li className="breadcrumb-item" aria-current="page">{adpost.ModelName}</li>
                                    
                                </ol>
                                
                            </nav>
                        </div>


                        <div className="row">
                            <div className="col-lg-12">
                            <div className="row justify-content-between">
                                <div className="list-heading">
                               
                                <span>{adpost.VehicleTypeName} {adpost.VehicleName} for Sale</span>
                               
                                </div>
                            </div>
                            </div>
                        </div>

                        <div className="demo">
                            <div className="item">
                            <div className="clearfix">
                                <ul id="image-gallery" className="gallery list-unstyled cS-hidden img-valign">
                                
                                {this.state.dataimage.map(image => (
                    
                                <li data-thumb={image.ImageUrl}>
                                    <img src={image.ImageUrl} />
                                </li>
                    
                                ))}    
                                
                                
                                </ul>
                            </div>
                            </div>
                        </div>


                        <div className="col-lg-12 col-md-6 col-sm-6 mt-3 mb-3">
                            <ul className="ad-detail-icon d-flex">
                            <li><span>YEAR</span><strong>{adpost.VersionYear}</strong></li>
                            <li><span>KILOMETERS</span><strong>{adpost.MeterReading} KM</strong></li>
                            <li><span>COLOR</span><strong> {adpost.ExteriorColor}</strong></li>
                            <li><span>DOORS</span><strong>{adpost.Doors} Doors</strong></li>
                            <li><span>TRANSMISSION</span><strong> {adpost.TransmissionName}</strong></li>
                            <li><span>BODY TYPE</span><strong> {adpost.BodyTypeName}</strong></li>
                            </ul>
                        </div>


                        <div>
                            <h5>Seller Notes</h5>
                            <div className="row mt-3">
                            <div className="col-md-12 col-12">
                                 <p>{adpost.Comments}</p>
                            </div>
                            </div>
                        </div>

                        <div>
                          <h5>Basic Details</h5>
                          <div className="row mt-3">
                            <div className="col-md-6">
                              <dl className="b-goods-f__descr row">
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>VERSION YEAR</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.VersionYear}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>BRAND</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.MakeName}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>MODEL</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.ModelName}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>BODY STYLE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.BodyTypeName}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>EXTERIOR COLOR</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.ExteriorColor}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE #</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.EngineNumber}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>CHASSIS #</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.ChasisNumber}</dt>
                                <hr className="col-12 mt-0  px-0" />
                              </dl>
                            </div>
                            <div className="col-md-6">
                              <dl className="b-goods-f__descr row">
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>VEHICLE TYPE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.VehicleTypeName}</dt>
                                <hr className="col-12 mt-0 px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>METER READING</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.MeterReading} Kms</dt>
                                <hr className="col-12 mt-0 px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE SIZE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.EngineSize_Capacity_CC} cc</dt>
                                <hr className="col-12 mt-0 px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF CYLINDERS</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.Cylinders} No(s).</dt>
                                <hr className="col-12 mt-0 px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>DRIVE SIDE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.DriveSide}</dt>
                                <hr className="col-12 mt-0 px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ASSEMBLY</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>
                                {adpost.Assembly == "IMP" ? 'IMPORTED':'LOCAL'}
                                </dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>REGISTERED CITY</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.RegisteredCity}</dt>
                                <hr className="col-12 mt-0  px-0" />
                              </dl>
                            </div>
                          </div>
                        </div>


                        <div>
                          <h5>Adtitional Info</h5>
                          <div className="row mt-3">
                            <div className="col-md-6">
                              <dl className="b-goods-f__descr row">
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>MILEAGE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.Mileage} Km/Ltr</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF DOORS</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.Doors} No(s).</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF SEATS</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.NumberofSeats} Pers</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF GEARS</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.NumberofGears} No(s).</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}># OF OWNERS</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.NumberofOwners}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>DRIVE TYPE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.DriveType}</dt>
                                <hr className="col-12 mt-0  px-0" />
                              </dl>
                            </div>
                            <div className="col-md-6">
                              <dl className="b-goods-f__descr row">
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE POWER</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.EnginePower} bhp</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>ENGINE TYPE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.EngineType}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>FUEL TANK SIZE</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.FuelTankCapacity} Ltr.</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>INTERIOR COLOR</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.InteriorColor}</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>BODY CONDITION</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.BodyCondition} out of 10</dt>
                                <hr className="col-12 mt-0  px-0" />
                                <dd className="b-goods-f__descr-new col-lg-6 col-md-12" style={{fontSize: '13px', color: '#222222'}}>TRANSMISSION</dd>
                                <dt className="b-goods-f__descr-info col-lg-6 col-md-12 px-0" style={{textAlign: 'right'}}>{adpost.TransmissionName}</dt>
                                <hr className="col-12 mt-0  px-0" />
                              </dl>
                            </div>
                          </div>
                        </div>

                        <h5>Features</h5>

                        <div className="row">
                        {this.state.dataatt.map(att => (
                          <div>
                            {att.AttributeName}
                          </div>
                        ))}
                        
                        </div>

                        <div className style={{backgroundColor: '#ff5500', textAlign: 'center'}}>
                          <div className="b-seller__title py-3">
                            <div className="b-seller__price"> {adpost.CurCode}  {adpost.Price}</div>
                            <span style={{color: 'white', fontSize: '11px', textAlign: 'center'}}>prices are exclusive of VAT</span>
                          </div>
                        </div>

                        <div className="b-goods-f__sidebar px-4 py-3" style={{backgroundColor: '#e0e0e0'}}>
                          <div className="b-goods-f__price-group justify-content-center">
                            <button type="button" onClick={this.handlephone} data-toggle="modal" data-target="#myphone_modal" className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <i style={{position: 'absolute', left: '45px'}} className="fas fa-phone prefix" /><span>Show Phone Number</span></button>
                            <button type="button" onClick={this.handleemail} data-toggle="modal" data-target="#myemail_modal" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <i style={{position: 'absolute', left: '45px'}} className="fas fa-envelope prefix " /><span>Show Email</span></button>
                            <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}>
                              <i style={{position: 'absolute', left: '45px'}} className="far fa-comment-dots prefix" /><span>Chat with seller</span>
                            </button>
                          </div>
                          <span className="b-goods-f__compare">
                          </span>
                        </div>

                        <div className="b-goods-f__sidebar px-4 py-2" style={{backgroundColor: '#e0e0e0', borderTop: '1px solid #cecece'}}>
                          <div className="b-goods-f__price-group justify-content-center">
                            {/* <span class="b-goods-f__price">
                                                        <span class="b-goods-f__price-numb">SAR &nbsp; 50,000</span>
                                                      </span> */}
                            <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <span>Installment Calculator</span></button>
                            <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center" style={{borderRadius: '0px', fontSize: '15px', fontWeight: 600}}> <span>Apply for Finance</span></button>
                          </div>
                          <span className="b-goods-f__compare">
                          </span>
                        </div>

                        <div className="b-goods-f__sidebar px-4 py-2" style={{backgroundColor: '#e0e0e0', borderTop: '1px solid #cecece'}}>
                          <div className="bg-detail-seller">
                            <div className="b-seller__detail pt-3 px-0 py-2" style={{backgroundColor: 'transparent'}}>
                              <div className="b-seller__img"><img className="img-scale" src={image1} alt="foto" /></div>
                              <div className="b-seller__title">
                                <div className="b-seller__name" style={{fontWeight: 600, fontSize: '16px'}}>{adpost.UserName}</div>
                                <div className="b-seller__category">Member Since {adpost.MemberSince}</div>
                              </div>
                              <hr />
                              <div className="b-seller__detail row d-flex justify-content-center px-0" style={{backgroundColor: 'transparent'}}>
                                <div className="mx-2">
                                  <i style={{color: '#ff5500', fontSize: '30px', paddingRight: '10px', transform: 'translateY(5px)'}} className="fas fa-phone" /><span style={{fontSize: '12px'}}> Phone verified</span>
                                </div>
                                <div className="mx-2"><i style={{color: '#ff5500', fontSize: '30px', paddingRight: '10px', transform: 'translateY(5px)'}} className="fas fa-envelope" /><span style={{fontSize: '12px'}}> E-mail verified</span></div>
                              </div>
                            </div>
                            <span className="b-goods-f__compare" style={{padding: '0px'}}>
                            </span>
                          </div>
                        </div>


                        <div className="my-3 pb-2 d-flex justify-content-center" style={{borderBottom: '1px solid #cecece'}}>
                          <div>
                            <a style={{color: 'grey'}} id="repo_id_@item.AdPostID" data-toggle="modal" data-target="#EmailSharetModal"><span style={{fontSize: '1.1em'}} className="mr-3 pb-2"><i className="far fa-envelope mr-1" /> Email this Ads</span></a>
                          </div>
                        </div>

                        
                        <div className="mt-3 finance" style={{backgroundColor: 'white', padding: '15px 30px', border: '1px solid #ff5500', boxShadow: '0 2px 3px 0 rgba(0, 0, 0, 0.1)', borderRadius: '8px'}}>
                          {/* finance calculator */}
                          <div className="mb-3">
                          <span className="finance-title" style={{color: '#363636', fontSize: '18px', fontWeight: 700, textTransform: 'uppercase'}}>Finance Calculator</span>
                          </div>
                          <div className="form-group" style={{fontSize: '13px', marginTop: '30px'}}>
                          <div className="row my-3">
                              <div className="col-6 align-self-center px-0">
                              <label htmlFor className="control-label" style={{fontWeight: 600}}>CAR PRICE* <span style={{color: '#ff5500'}}>(SAR)</span></label>
                              </div>
                              <div className="col-6">
                              <input className="form-control" type="text" onChange={this.handlepricechange} value={adpost.Price} required disabled="disabled" />
                              </div>
                          </div>
                          <div className="row my-3">
                              <div className="col-6 align-self-center px-0">
                              <label htmlFor className="control-label" style={{fontWeight: 600}}>DOWN PAYMENT* <span style={{color: '#ff5500'}}>(%)</span></label>
                              </div>
                              <div className="col-6">
                              <input className="form-control" placeholder="Enter" style={{color: '#363636'}} type="text" onChange={this.handledpaychange} value={this.state.dpay}  required />
                              </div>
                          </div>
                          <div className="row my-3">
                              <div className="col-6 align-self-center px-0">
                              <label htmlFor className="control-label" style={{fontWeight: 600}}>TENURE* <span style={{color: '#ff5500'}}>(YEAR)</span></label>
                              </div>
                              <div className="col-6">
                              <input className="form-control" placeholder="Enter" style={{color: '#363636'}} type="text" onChange={this.handletenurechange} value={this.state.tenure}  required />
                              </div>
                          </div>
                          <div className="row my-3">
                              <div className="col-6 align-self-center px-0">
                              <label htmlFor className="control-label" style={{fontWeight: 600}}>INTEREST RATE* <span style={{color: '#ff5500'}}>(%)</span></label>
                              </div>
                              <div className="col-6">
                              <input className="form-control" placeholder="Enter" style={{color: '#363636'}} type="text" onChange={this.handleiratechange} value={this.state.irate} required />
                              </div>
                          </div>
                          <div className="pt-3">
                              <button type="submit" onClick={this.handlecalcuclick} id={adpost.Price} role="button" className="btn btn-block" style={{border: 'none', backgroundColor: '#ff5500', color: 'white'}}>Calculate</button>
                          </div>
                          </div>
                          <div id="fc-result">
                          <div className="input full my-3" style={{padding: '20px', backgroundColor: '#495057', color: 'white', fontSize: '15px', fontWeight: 700, WebkitBoxShadow: '0 2px 3px 0 rgba(0, 0, 0, 0.2)', borderRadius: '8px'}}>
                              <div className="row mb-3">
                              <div className="col-6" style={{borderRight: '1px solid grey'}}> <label className="string optional" style={{textAlign: 'left', marginBottom: '0px', lineHeight: '1.3'}}>Monthly Installment</label> </div>
                              <div className="col-6 align-self-center">
                                                    <label className="string optional my-0" style={{textAlign: 'left', fontSize: '17px'}}> <span style={{fontSize: '12px'}} id="ID_Emi">SAR {this.state.emi}</span></label>
                              </div>
                              </div>
                              <hr style={{borderTop: '1px solid #e9ecef38'}} />
                              <div className="row mb-3">
                              <div className="col-6" style={{borderRight: '1px solid grey'}}> <label className="string optional" style={{textAlign: 'left', marginBottom: '0px', lineHeight: '1.3'}}>Total Amount</label> </div>
                              <div className="col-6 align-self-center">
                                  <label className="string optional my-0" style={{textAlign: 'left', fontSize: '17px'}}> <span style={{fontSize: '12px'}} id="ID_Tot_Amt">SAR {this.state.totalamountto}</span></label>
                              </div>
                              </div>
                              <hr style={{borderTop: '1px solid #e9ecef38'}} />
                              <div className="row">
                              <div className="col-6" style={{borderRight: '1px solid grey'}}> <label className="string optional" style={{textAlign: 'left', marginBottom: '0px', lineHeight: '1.3'}}>Total Interest Amount</label> </div>
                              <div className="col-6 align-self-center">
                                  <label className="string optional my-0" style={{textAlign: 'left', fontSize: '18px'}}> <span style={{fontSize: '12px'}} id="ID_Tot_IRat">SAR {this.state.totalinterestamount}</span></label>
                              </div>
                              </div>
                          </div>
                          </div>
                      </div> 


                    </div>
                ))}
                
 
      </div>
    );
  }
}

export default Adpostdetailmain;