import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';

class Adpostdetailsimilar extends Component {
    constructor(props)
    {
        super(props);
        this.state = { llisting: [], toplist:[], error: null, validationError: '' };
    }

    componentDidMount() {
        const apiUrlListing = 'http://cararabiya.ahalfa.com/api/Listing/AllListing';
        
        fetch(apiUrlListing)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                llisting: result
              });
              if(result.length > 5)
              {
                var newlist=[];
                for(var i=0;i<5;i++)
                {
                  newlist.push(result[i])
                }
                this.setState({
                  toplist: newlist
                });
              }
              else
              {
                this.setState({
                    toplist: result
                  });
              }
              

            },
            (error) => {
              this.setState({ error });
            }
          )
      }
    render() {
        return (
            <div>
                 {this.state.toplist.map(toplist => (  
                    <div className="b-goods-f col-lg-4 col-md-6">
                        <div className="b-goods-f__media">
                            <img className="b-goods-f__img img-scale img-valign" height="100px" width="100px" src={toplist.ImageUrl} alt={toplist.AdPostID} />
                        </div>
                        <div className="b-goods-f__main bg-shadow">
                            <div class="b-goods-f__title_myad">
                                <a href="#">
                                    <div className="b-goods-f__title_myad">{toplist.Vehicle}</div>
                                </a>
                                <div className="b-goods-f__info"></div>
                                <ul className="b-goods-f__list list-unstyled row justify-content-between">
                                    <div>
                                        <div>
                                            <div>
                                                <i className="fas fa-calendar-alt" title="Model Year"></i>
                                            </div>
                                            <div>
                                                <span className="b-goods-f__list-info">{toplist.VersionYear}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <div>
                                                <i className="fas fa-tachometer-alt" title="MeterReading Km/Ltr"></i>
                                            </div>
                                            <div>
                                                <span className="b-goods-f__list-info" >{toplist.MeterReading}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <div>
                                                <i className="fas fa-car-alt" title="Car Type"></i>
                                            </div>
                                            <div>
                                                <span className="b-goods-f__list-info">New</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <div>
                                                <i title="Mileade Km/Ltr" className="fas fa-gas-pump"></i>
                                            </div>
                                            <div>
                                                <span className="b-goods-f__list-info">{toplist.Mileage}</span>
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                            <div className="b-goods-f__sidebar">
                            <Link to="/" className="b-goods-f__bnr"  ><span className="b-goods-f__price-group"><span className="b-goods-f__pricee"><span className="b-goods-f__price-numbb">{toplist.CurCode} {toplist.Price}</span></span></span></Link>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

export default Adpostdetailsimilar;