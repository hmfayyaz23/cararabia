import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Promosearch extends Component {

    constructor(props) {
        super(props);
        this.state = { searchkey: '', ldealer: [], sdealer:[], lcity: [], scity:[], error: null, validationError: '' };

        this.handlesearchkeyChange = this.handlesearchkeyChange.bind(this);
        this.handlecityChange = this.handlecityChange.bind(this);
        this.handlesearch = this.handlesearch.bind(this);
        this.handledealerChange = this.handledealerChange.bind(this);
        this.handleqsearch = this.handleqsearch.bind(this);
    }

    handlesearchkeyChange(event) {
        const searchkeyname = event.target.name;
        const searchkeyvalue = event.target.value;
    
        this.setState({
          [searchkeyname]: searchkeyvalue
        })
      }

      handlecityChange(event) {
        const searchkeyid = event.target.id;
        const searchkeyvalue = event.target.value;
        const cheked= event.target.checked;
        //var joined = this.state.scity.concat('abc');
        //this.setState({ scity: joined });
        
        if(cheked == true)
        {
            var joined = this.state.scity;
            joined.push(searchkeyid);
            this.setState({ scity: joined });
        }
        else
        {
            let filteredArray = this.state.scity.filter(item => item !== searchkeyid)
            this.setState({scity: filteredArray});
        }    
     
      }

      handledealerChange(event) {
        const searchkeyid = event.target.id;
        const searchkeyvalue = event.target.value;
        const cheked= event.target.checked;
        //var joined = this.state.scity.concat('abc');
        //this.setState({ scity: joined });
        
        if(cheked == true)
        {
            var joined = this.state.sdealer;
            joined.push(searchkeyid);
            this.setState({ sdealer: joined });
        }
        else
        {
            let filteredArray = this.state.sdealer.filter(item => item !== searchkeyid)
            this.setState({sdealer: filteredArray});
        }
        
        
     
      }

      handlesearch(event) {
        
      }

      handleqsearch(event) {
        
    }

      componentDidMount() {
        
        const apiUrlCity = 'http://cararabiya.ahalfa.com/api/city?countryID=184';
        const apiUrldealer = 'http://cararabiya.ahalfa.com/api/Dealer';
    
        
          fetch(apiUrlCity)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                lcity: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )

          fetch(apiUrldealer)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                ldealer: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )


      }

    render() {
        const image1="http://cararabiyaweb.ahalfa.com/Content/assets/media/content/ad/ad3.jpg";
        
        return (
           

<aside className="l-sidebar">
<div className="widget widget-search section-sidebar ">
  <div className="b-filter-slider__title" style={{fontSize: '14px'}}>Search by keyword</div>
  <form className="form-sidebar-adv row" id="search-global-form">
     <input className="form-sidebar-adv__input form-control" name="searchkey" value={this.state.searchkey} onChange={this.handlesearchkeyChange} placeholder="Enter Keyword"/>                            
      <button className="form-sidebar-adv__btn" onClick={this.handlesearch}><i className="ic icon-magnifier"></i></button>         
  </form>        
</div>
<div className="accordion" id="accordion-2">
  <div className="card">
    <div className="card-header px-0 py-0" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
      <h3 className="widget-title" style={{backgroundColor: '#e8e8e8', color: '#253241', textTransform: 'none', fontSize: '16px', fontWeight: 600}}>Quick Search <button><i class="fas fa-redo-alt"></i></button></h3>
    </div>
    <div className="accordion-body" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion-2">
      <div className="card-body">
        <div className="widget-inner">
          <div className="widget widget-search section-sidebar">
          <div className="b-filter-slider__title">City</div>  
                {this.state.lcity.map(city => (
                    <div className="form-check col-12">
                        <input type="checkbox" id={city.CityID} value={city.CityName} onChange={this.handlecityChange}/>&nbsp;{city.CityName}
                    </div>
                ))}
          
                <br />
           <div className="b-filter-slider__title">Dealers</div>  
       
                {this.state.ldealer.map(dealer => (
                    <div className="form-check col-12">
                        <input type="checkbox" id={dealer.dealerID} value={dealer.dealerName} onChange={this.handledealerChange}/>&nbsp;{dealer.dealerName}
                    </div>
                ))}

            <hr />
          </div>
          <div id="special">
                    <Link className="btn btn-primary btn-block" onClick={this.handleqsearch}>Search</Link>
                </div>
         
        </div>
       
      </div>  
     
    </div>
  
  </div>  
  
</div>

</aside>
        );
    }
}

export default Promosearch;