import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';

class Promolatestads extends Component {
    constructor(props)
    {
        super(props);
        this.state = { llisting: [], toplist:[], error: null, validationError: '' };
    }

    componentDidMount() {
        const apiUrlListing = 'http://cararabiya.ahalfa.com/api/Listing/AllListing';
        
        fetch(apiUrlListing)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                llisting: result
              });
              if(result.length > 3)
              {
                var newlist=[];
                for(var i=0;i<3;i++)
                {
                  newlist.push(result[i])
                }
                this.setState({
                  toplist: newlist
                });
              }
              else
              {
                this.setState({
                    toplist: result
                  });
              }
              

            },
            (error) => {
              this.setState({ error });
            }
          )
      }
    render() {
        return (
            <div>
               
<main className="b-goods-group row feature-toggle-top">
{this.state.toplist.map(toplist => (
          <div className="b-goods-f col-lg-4 col-md-6">
            <div className="b-goods-f__media"><img className="watermark-recent" src="./assets/media/watermark.png" /><img className="b-goods-f__img img-scale" style={{maxHeight: '220px'}} src={toplist.ImageUrl} alt={toplist.AdPostID} /></div>
            <div className="b-goods-f__main bg-shadow">
              <div className="b-goods-f__descrip">
                <div className="b-goods-f__title b-goods-f__title_myad" style={{marginBottom: '0px'}}>
                  <Link style={{fontSize: '18px', fontWeight: 600}} href="Detailed-Ad.html">{toplist.Vehicle}</Link>
                </div>
                <div className="b-goods-f__info">Magna aliqua enim aduas veniam quis nostrud exercitation ullam laboris aliquip.</div>
                <ul className="b-goods-f__list list-unstyled row justify-content-between" style={{fontSize: '11px', padding: '5px 20px', marginTop: '0px'}}>
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-calendar-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>{toplist.VersionYear}</span>
                      </div>
                    </div>
                  </div>   
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-tachometer-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>{toplist.MeterReading}</span>
                      </div>
                    </div>
                  </div>   
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-car-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>Used</span>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-gas-pump" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>{toplist.Mileage}</span>
                      </div>
                    </div>
                  </div>     
                </ul>
              </div>
              <div className="b-goods-f__sidebar" style={{paddingTop: '10px', borderTop: '1px solid #ddd'}}><Link className="b-goods-f__bnr" href="#"><img src="assets/media/content/b-goods/auto-check.png" alt="auto check" /></Link><span className="b-goods-f__price-group"><span className="b-goods-f__pricee"><span className="b-goods-f__price_col">PRICE:&nbsp;</span><span className="b-goods-f__price-numbb">{toplist.CurCode} {toplist.Price}</span></span></span>   
              </div>
            </div>
          </div>   
           ))} 
        </main>
            </div>
        );
    }
}

export default Promolatestads;