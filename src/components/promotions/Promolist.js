import React, { Component } from 'react';

class Promolist extends Component {
    render() {
        return (
            <div>
                <div className="b-filter-goods pt-3">
          {/*   <div class="b-filter-goods__info col-auto">Showing results<strong> 1 to 10</strong> of total<strong> 145</strong></div> */}
          <div className="row justify-content-between">
            <div className="list-heading">Offers &amp; Promotions
            </div>
            <div className="b-filter-goods__wrap col-auto">
              <div className="b-filter-goods__select">
                <select className="custom-select-adpost" placeholder="Sort by newest">
                  <option>Sort by</option>
                  <option>newest</option>
                  <option>oldest</option>
                </select>
              </div> 
            </div>
          </div>
        </div>
        <div className="col-12 line-o  my-3 px-0"> {/* Listing start
 */}
          <div className="row">
            <div className="col-12 px-0">
              <div className="row">
                <div className="col-12 col-md-4">
                  <div className="b-goods-f__listing mb-2 pl-3 pb-3">
                    <a href="Detailed-Ad.html">
                      <img className="b-goods-f__img img-scale" src="assets/media/promo-1.jpg" alt="" />
                    </a>
                  </div>
                </div>
                <div className="col-12 col-md-8">
                  <div className="b-goods-f__main">
                    <div className="b-goods-f__descrip">
                      <div className="offers-f__title_myad d-flex justify-content-between">
                        <a href="Detailed-Ad.html">ABDULLAH DEALERS</a>
                        <div><a className="special" role="link" href="#" style={{fontSize: '0.7em', fontWeight: 400, marginRight: '10px'}}>Visit Website</a>
                          <i className="far fa-thumbs-up px-1" />
                          <i className="far fa-thumbs-down" />
                        </div>
                      </div>
                      <div className="b-goods-f__info mt-2" style={{fontSize: '1.1em', fontWeight: 600, position: 'relative', top: '-2px', backgroundColor: '#f7f3db', padding: '5px'}}>HOLIDAY SALES PROMOTION</div>
                      <div className="b-goods-f__info mt-2" style={{fontSize: '0.9em', position: 'relative', top: '-2px', backgroundColor: 'white', padding: '5px'}}>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors.
                      </div>
                      <div className="d-flex justify-content-between" style={{color: 'grey', fontSize: '0.9em'}}>
                        <div className="align-self-center"> 
                          <span className="ml-2"><b>Date posted:</b> 19 Dec 2019</span>
                          <span className="ml-2"><b>Valid till:</b> 19 Jan 2020</span>
                        </div>
                        <div>
                          <button className="btn btn-primary cararabiya-color py-2" data-toggle="modal" data-target="#redeemModal">Redeem</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> {/* end row */}
        </div>  {/* listing end */}                 
        <br />
        <nav aria-label="Page navigation">
          <ul className="pagination justify-content-center">
            <li className="page-item active" aria-current="page"><a className="page-link" href="#">1</a></li>
            <li className="page-item"><a className="page-link" href="#">2</a></li>
            <li className="page-item"><a className="page-link" href="#">3</a></li>
            <li className="page-item"><a className="page-link" href="#">4</a></li>
            <li className="page-item"><a className="page-link" href="#">5</a></li>
            <li className="page-item"><a className="page-link" href="#" aria-label="Next"><span className="ic fas fa-angle-double-right" aria-hidden="true" /></a></li>
          </ul>
        </nav>
        <br />
        <br />
            </div>
        );
    }
}

export default Promolist;