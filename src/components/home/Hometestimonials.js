import React, { Component } from "react";
import Star from "./Hometeststar";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 1, // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 1, // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
};

class Hometestimonials extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ltestimonials: [],
      toptestimonials: [],
      error: null,
      validationError: "",
    };
  }
  componentDidMount() {
    const apiUrlListing = "http://cararabiya.ahalfa.com/api/AppTestimonial";

    fetch(apiUrlListing)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            ltestimonials: result,
          });
          if (result.length > 10) {
            var newlist = [];
            for (var i = 0; i < 10; i++) {
              newlist.push(result[i]);
            }
            this.setState({
              toptestimonials: newlist,
            });
          } else {
            this.setState({
              toptestimonials: result,
            });
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  changeColor = (currentSlide) => {

    this.refs.reviews.style.background = 'red'; 
    console.log(this.refs.reviews.style, currentSlide);
  }
  render() {
    return (
      // <div>
      //   <h2 className="ui-title">TESTIMONIALS</h2>

      //   {this.state.toptestimonials.map(toplist => (<div>
      //     <span className="section-reviews__decor">“</span>
      //     <div className="b-reviews">
      //       <blockquote className="b-reviews__blockquote">
      //         <div className="b-reviews__wrap">
      //           <Star number={toplist.StarsCount}></Star>
      //           <p>{toplist.ReviewText}</p>
      //         </div>
      //         <cite className="b-reviews__cite" ><span className="b-reviews__inner"><span className="b-reviews__name">{toplist.UserName}</span></span><span className="b-reviews__author"></span></cite>
      //       </blockquote>
      //     </div>

      //   </div>))}
      // </div>
      <div>
        <a id="_testimonial" name="_testimonial" />
        <section className="section-reviews section-default parallax area-bg area-bg_dark">
          <div className="area-bg__inner">
            <div className="container-fluid">
              <div className="row">
                <div className="col-12">
                  <div className="text-center">
                    <h2 className="ui-title">TESTIMONIALS</h2>
                    <span className="section-reviews__decor">“</span>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-12">
                
                    <Carousel
                    beforeChange={(nextSlide, { currentSlide, onMove }) => {
                      this.refs.reviews.className = ''; 
      
                    }}
                      additionalTransfrom={0}
                      arrows={false}
                      swipeable={false}
                      draggable={false}
                      showDots={true}
                      centerMode
                      responsive={responsive}
                      customTransition="all 1s linear"
                      ssr={true} // means to render carousel on server-side.
                      infinite={true}
                      autoPlay={true}
                      focusOnSelect={false}
                      autoPlaySpeed={2000}
                      keyBoardControl={true}
                      transitionDuration={500}
                      containerClass=""
                      removeArrowOnDeviceType={["tablet", "mobile"]}
                      deviceType={this.props.deviceType}
                      dotListClass="custom-dot-list-style"
                      itemClass="carousel-item-padding-40-px"
                    >
                      {this.state.toptestimonials.map((toplist, index) => (
                        <div className="b-reviews">
                          <blockquote className="b-reviews__blockquote">
                            <div className="b-reviews__wrap" ref="reviews">
                              <div className="mb-1">
                                <Star number={toplist.StarsCount}></Star>
                              </div>
                              <p>{toplist.ReviewText}</p>
                            </div>
                            <cite
                              className="b-reviews__cite"
                              title="Blockquote Title"
                            >
                              <span className="b-reviews__inner">
                                <span className="b-reviews__name">
                                  {toplist.UserName}
                                </span>
                                <span className="b-reviews__category">
                                  Riyadh
                                </span>
                              </span>
                            </cite>
                          </blockquote>
                        </div>
                      ))}
                    </Carousel>
              
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Hometestimonials;