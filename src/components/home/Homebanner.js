import React, { Component } from "react";
import { Carousel } from "react-bootstrap";

class Homebanner extends Component {
  render() {
    const image1 =
      "http://cararabiyaweb.ahalfa.com/Content/assets/banner-horizontal.png";
    const image2 =
      "http://cararabiyaweb.ahalfa.com/Content/assets/banner-horizontal-2.png";

    return (
      <div className="container-fluid">
        <div className="container pt-5 pb-5">
          <Carousel interval={2000}>
            <Carousel.Item>
              <img className="d-block w-100" src={image1} alt="First slide" />
            </Carousel.Item>

            <Carousel.Item>
              <img className="d-block w-100" src={image2} alt="First slide" />
            </Carousel.Item>
          </Carousel>
        </div>
      </div>
    );
  }
}

export default Homebanner;
