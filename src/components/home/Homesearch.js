import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./HomeComponent.css";

class Homesearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sbrand: "",
      stype: "",
      scity: "",
      lbrand: [],
      ltype: [],
      lcity: [],
      error: null,
      validationError: ""
    };
  }

  componentDidMount() {
    const apiUrlMake = "http://cararabiya.ahalfa.com/api/make";
    const apiUrlCity = "http://cararabiya.ahalfa.com/api/city?countryID=184";
    const apiUrltype = "http://cararabiya.ahalfa.com/api/VehicleType";

    fetch(apiUrlMake)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            lbrand: result
          });
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlCity)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            lcity: result
          });
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrltype)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            ltype: result
          });
        },
        error => {
          this.setState({ error });
        }
      );
  }

  render() {
    return (
      <div className="container-fluid bg-orange">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="b-find b-find_sm">
                <form className="b-find__form">
                  <div className="b-find__row">
                    <div className="b-find__main pr-0">
                      <div className="b-find__inner responsive-800">
                        <div className="b-find__item">
                          <div className="b-find__selector search_text_color">
                            <select data-width="100%" value={this.state.sbrand}>
                              {this.state.lbrand.map(brand => (
                                <option key={brand.MakeID} value={brand.MakeID}>
                                  {brand.MakeName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="b-find__item">
                          <div className="b-find__selector">
                            <select value={this.state.stype}>
                              {this.state.ltype.map(type => (
                                <option
                                  key={type.VehicleTypeID}
                                  value={type.VehicleTypeID}
                                >
                                  {type.VehicleTypeName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="b-find__item">
                          <div className="b-find__selector">
                            <select value={this.state.scity}>
                              {this.state.lcity.map(city => (
                                <option key={city.CityID} value={city.CityID}>
                                  {city.CityName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="b-find__item">
                          <div className="b-find__selector">
                            <button className="b-find__btn btn btn-primary">
                              <Link to="/">Search</Link>
                              <i className="fas fa-search pl-2" />
                            </button>
                          </div>
                        </div>
                        <div className="b-find__item d-flex justify-content-center align-items-center">
                          <div id="advsearch" className="b-find__selector">
                            <Link
                              to="/Advancesearch"
                              style={{ borderBottom: "1px solid white" }}
                            >
                              Advance Search
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homesearch;