import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import "./HomeComponent.css";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 6,
    slidesToSlide: 3 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

class Homepromo extends Component {
  constructor(props) {
    super(props);
    this.state = { lbanks: [], lpromos: [], lshop: [], error: null };
  }

  componentDidMount() {
    const apiUrlBanks = "http://cararabiya.ahalfa.com/api/bank";
    const apiUrlPromos = "http://cararabiya.ahalfa.com/api/PromoAndOffer";

    fetch(apiUrlBanks)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            lbanks: result
          });
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlPromos)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            lpromos: result
          });
        },
        error => {
          this.setState({ error });
        }
      );
  }

  render() {
    console.log(this.state.lpromos);
    const image1 =
      "http://cararabiyaweb.ahalfa.com/Content/assets/media/content/b-shop/3m.png";
    return (
      <div className="section-area bg-promotions car-pattern">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="b-find">
                <ul
                  className="b-find-nav nav nav-tabs"
                  id="findTab special-tab"
                  role="tablist"
                  style={{ borderBottom: "0px", marginBottom: 20 }}
                >
                  <li className="nav-item">
                    <span className="b-find-nav__link nav-linkk disabled pl-0">
                      Offers &amp; Promotions
                    </span>
                  </li>
                </ul>

                <Tabs
                  className="justify-content-center"
                  defaultActiveKey="BANKS"
                  id="uncontrolled-tab-example"
                  style={{ display: "flex" }}
                >
                  <Tab eventKey="BANKS" title="BANKS">
                    <div className="tabContainer">
                      <Carousel
                        additionalTransfrom={0}
                        arrows={false}
                        swipeable={false}
                        draggable={false}
                        showDots={false}
                        centerMode={false}
                        responsive={responsive}
                        customTransition="all 1s linear"
                        ssr={true} // means to render carousel on server-side.
                        infinite={true}
                        autoPlay
                        autoPlaySpeed={2000}
                        keyBoardControl={true}
                        transitionDuration={500}
                        containerClass="carousel-container"
                        removeArrowOnDeviceType={["tablet", "mobile"]}
                        deviceType={this.props.deviceType}
                        dotListClass="custom-dot-list-style"
                        itemClass="carousel-item-padding-40-px"
                      >
                        {this.state.lbanks.map((lbank, index) => (
                          <img src={lbank.ImageUrl} key={index} alt="lbank" />
                        ))}
                      </Carousel>
                    </div>
                  </Tab>

                  <Tab eventKey="AUTO DEALERS" title="AUTO DEALERS">
                  <div className="tabContainer">
                      <Carousel
                        additionalTransfrom={0}
                        arrows={false}
                        swipeable={false}
                        draggable={false}
                        showDots={false}
                        centerMode={false}
                        responsive={responsive}
                        customTransition="all 1s linear"
                        ssr={true} // means to render carousel on server-side.
                        infinite={true}
                        autoPlay
                        autoPlaySpeed={2000}
                        keyBoardControl={true}
                        transitionDuration={500}
                        containerClass="carousel-container"
                        removeArrowOnDeviceType={["tablet", "mobile"]}
                        deviceType={this.props.deviceType}
                        dotListClass="custom-dot-list-style"
                        itemClass="carousel-item-padding-40-px"
                      >
                        {this.state.lpromos.map((lpromo, index) => (
                          <img src={lpromo.ImageUrl} key={index} alt="lpromo" style={{width:127, height:75}} />
                        ))}
                      </Carousel>
                    </div>
                  </Tab>

                  <Tab eventKey="SHOP" title="SHOP">
                  <div className="tabContainer">
                      <Carousel
                        additionalTransfrom={0}
                        arrows={false}
                        swipeable={false}
                        draggable={false}
                        showDots={false}
                        centerMode={false}
                        responsive={responsive}
                        customTransition="all 1s linear"
                        ssr={true} // means to render carousel on server-side.
                        infinite={true}
                        autoPlay
                        autoPlaySpeed={2000}
                        keyBoardControl={true}
                        transitionDuration={500}
                        containerClass="carousel-container"
                        removeArrowOnDeviceType={["tablet", "mobile"]}
                        deviceType={this.props.deviceType}
                        dotListClass="custom-dot-list-style"
                        itemClass="carousel-item-padding-40-px"
                      >
                        {this.state.lpromos.map((lpromo, index) => (
                          <img src={lpromo.ImageUrl} key={index} alt="lpromo" style={{width:127, height:75}} />
                        ))}
                      </Carousel>
                    </div>
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homepromo;
