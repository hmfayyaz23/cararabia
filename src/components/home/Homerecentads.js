import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

class Homerecentads extends Component {
  constructor(props) {
    super(props);
    this.state = {
      llisting: [],
      toplist: [],
      error: null,
      validationError: "",
    };
  }

  componentDidMount() {
    const apiUrlListing = "http://cararabiya.ahalfa.com/api/Listing/AllListing";

    fetch(apiUrlListing)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            llisting: result,
          });
          if (result.length > 4) {
            var newlist = [];
            for (var i = 0; i < 4; i++) {
              newlist.push(result[i]);
            }
            this.setState({
              toplist: newlist,
            });
            console.log(this.state.toplist);
          } else {
            this.setState({
              toplist: result,
            });
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  render() {
    console.log(this.state.toplist);
    return (
      <div>
        <div className="container">
          <div className="margin">
            <div className="row">
              <div className="col-8 col-xl-10 col-lg-10 col-md-10 col-sm-9">
                <h4 className="ui-title-promo">Most Recent Ads</h4>
              </div>
              <div className="col-4 col-xl-2 col-lg-2 col-md-2 col-sm-3 pt-3">
                <a
                  className="btn btn-primary btn-sm"
                  href="#"
                  style={{ backgroundColor: "#ff5500" }}
                >
                  View all
                </a>
              </div>
            </div>

            <div className="row">
              {this.state.toplist.map((toplist) => (
                <div className="col-md-3">
                  <div className="b-goods-f">
                    <div className="b-goods-f__media">
                      <img
                        className="b-goods-f__img img-scale"
                        src="http://cararabiya.ahalfa.com/Content/Uploads/505/Transpo_G70_TA-518126.jpg"
                        //src={toplist.ImageUrl}
                        alt="foto"
                        style={{ height: "200px" }}
                      />
                      <span className="b-goods-f__media-inner">
                        <span className="b-goods-f__favorite">
                          <i className="ic far fa-star" />
                        </span>
                        <span className="b-goods-f__label bg-primary">
                          Featured
                        </span>
                      </span>
                    </div>
                    <div className="b-goods-f__main bg-shadow">
                      <div className="b-goods-f__descrip">
                        <div
                          className="b-goods-f__title b-goods-f__title_myad"
                          style={{ marginBottom: "0px" }}
                        >
                          <Link
                            style={{ fontSize: "18px", fontWeight: 600 }}
                            to="/"
                          >
                            {toplist.Vehicle}
                          </Link>
                        </div>

                        <ul
                          className="b-goods-f__list list-unstyled row"
                          style={{
                            fontSize: "11px",
                            padding: "5px 10px",
                            marginTop: "0px",
                            justifyContent: 'space-evenly',
                          }}
                        >
                          <div class="row">
                            <div className="col-md-3" style={{padding: 2}} >
                              <div>
                                <i
                                  className="fas fa-calendar-alt"
                                  style={{ color: "#ff5500", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.VersionYear}
                                </span>
                              </div>
                            </div>

                            <div className="col-md-3" style={{padding: 2}}>
                              <div>
                                <i
                                  className="fas fa-tachometer-alt"
                                  style={{ color: "#ff5500", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.Mileage} Km/Ltr
                                </span>
                              </div>
                            </div>

                            <div className="col-md-3" style={{padding: 2}}>
                              <div>
                                <i
                                  className="fas fa-car-alt"
                                  style={{ color: "#ff5500", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.TransmissionName}
                                </span>
                              </div>
                            </div>

                            <div className="col-md-3" style={{padding: 2}}>
                              <div>
                                <i
                                  className="fas fa-gas-pump"
                                  style={{ color: "#ff5500", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.FuelTypeName}
                                </span>
                              </div>
                            </div>
                          </div>
                        </ul>
                      </div>

                      <div
                        className="b-goods-f__sidebar"
                        style={{
                          paddingTop: "10px",
                          borderTop: "1px solid #ddd",
                        }}
                      >
                        <a className="b-goods-f__bnr" href="#">
                          <img
                            src="./assets/media/content/b-goods/auto-check.png"
                            alt="auto check"
                          />
                        </a>
                        <span className="b-goods-f__price-group">
                          <span className="b-goods-f__pricee">
                            <span className="b-goods-f__price_col">
                              PRICE:&nbsp;
                            </span>
                            <span className="b-goods-f__price-numbb">
                              {toplist.CurCode} {toplist.Price}
                            </span>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homerecentads;