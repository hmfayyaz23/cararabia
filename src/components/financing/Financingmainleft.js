import React, { Component } from 'react';

class Financingmainleft extends Component {
    constructor(props)
    {
        super(props);
        this.state = { 
            carprice: '', 
            downpayment: '', 
            tenure: '', 
            interestrate: '', 
            emi: '', 
            emidownpayment: '', 
            emitenure: '', 
            emiinterestrate: '', 
            emiP: '',
            emiPA: '',
            emiTIA: '',
            emiTA: '',
            priceMI: '',
            priceTA: '',
            priceTIA: '', 
            error: null, 
            validationError: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handlepriceonClick = this.handlepriceonClick.bind(this);
        this.handleemionClick = this.handleemionClick.bind(this);
        this.handleisNumberKey = this.handleisNumberKey.bind(this);
        
        
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
    
        this.setState({
          [name]: value
        })
      }

      handlepriceonClick(event) {

        var txt_ID_Price = this.state.carprice;
        var txt_ID_DPay = this.state.downpayment;
        var txt_ID_Tenure = this.state.tenure;
        var txt_ID_IRate = this.state.interestrate;

        var Tenure = txt_ID_Tenure * 12;
        var InterestRate = (txt_ID_IRate / 100) / 12;
        var DownPayment = txt_ID_DPay / 100;
        var FinanceAmount = txt_ID_Price * (1 - DownPayment);

        var I = 1 + InterestRate;
        var y = Math.pow(I, Tenure);
        var yNew = y * InterestRate;


        var z = Math.pow((1 + InterestRate), Tenure);
        var zNew = z - 1;
        var xvalue = yNew / zNew;
        var EMI = Math.round(FinanceAmount * xvalue);
        var PrincipleAmount = Math.round((EMI / xvalue) / (1 - DownPayment));
        var TotalAmountto = Math.round(EMI * Tenure);
        var DownpaymentAmount = PrincipleAmount * DownPayment;
        var TotalInterestAmount = Math.round(TotalAmountto * InterestRate * 12);

        //const output11= '<div><div>Monthly Installment</div></div><div><div>'+EMI+'</div></div>';
        //output11 = '<div><div>Total Amount</div></div><div><div>'+TotalAmountto+'</div></div>';
        //output11 = '<div><div>Total Interest Amount</div></div><div><div>'+TotalInterestAmount+'</div></div>';

        
        //$("#ID_Emi").text(EMI);
        //$("#ID_Tot_Amt").text(TotalAmountto);
        //$("#ID_Tot_IRat").text(TotalInterestAmount);

        this.setState({
            priceMI: EMI,
            priceTA: TotalAmountto,
            priceTIA: TotalInterestAmount
          })

        
      }

      handleemionClick(event) {

        var txt_ID_Price = this.state.emi;
        var txt_ID_DPay = this.state.emidownpayment;
        var txt_ID_Tenure = this.state.emitenure;
        var txt_ID_IRate = this.state.emiinterestrate;

        var Tenure = txt_ID_Tenure * 12;
        var InterestRate = (txt_ID_IRate / 100) / 12;
        var DownPayment = txt_ID_DPay / 100;
        var FinanceAmount = txt_ID_Price * (1 - DownPayment);

        var I = 1 + InterestRate;
        var y = Math.pow(I, Tenure);
        var yNew = y * InterestRate;


        var z = Math.pow((1 + InterestRate), Tenure);
        var zNew = z - 1;
        var xvalue = yNew / zNew;
        var EMI = Math.round(txt_ID_Price * xvalue);
        var PrincipleAmount = Math.round((txt_ID_Price / xvalue) / (1 - DownPayment));
        var TotalAmountto = Math.round(txt_ID_Price * Tenure);
        var DownpaymentAmount = PrincipleAmount * DownPayment;
        var TotalInterestAmount = Math.round(TotalAmountto * InterestRate *12);
        
       // const output= '<div><div>Monthly Installment</div></div><div><div>'+txt_ID_Price+'</div></div>';
       // output += '<div><div>Car Amount</div></div><div><div>'+PrincipleAmount+'</div></div>';
       // output += '<div><div>Total Amount</div></div><div><div>'+TotalInterestAmount+'</div></div>';
       // output += '<div><div>Total Interest Amount</div></div><div><div>'+TotalAmountto+'</div></div>';
        
        //$("#ID_Emi2").text(txt_ID_Price);
        //$("#ID_Tot_Amt2").text(PrincipleAmount);
        //$("#ID_Tot_IRat2").text(TotalInterestAmount);
        //$("#ID_Tot_PayA2").text(TotalAmountto);

        this.setState({
            emiP: txt_ID_Price,
            emiPA: PrincipleAmount,
            emiTIA: TotalInterestAmount,
            emiTA: TotalAmountto
          })
    }

    handleisNumberKey(event){
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }




    render() {
      
        return (
            <div>
                <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="pills-price-tab" data-toggle="pill" href="#pills-price" role="tab" aria-controls="pills-price" aria-selected="true">Calculate by Price</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="pills-emi-tab" data-toggle="pill" href="#pills-emi" role="tab" aria-controls="pills-emi" aria-selected="false">Calculate by EMI</a>
                    </li>
                </ul>
                    <div className="tab-pane fade show active" id="pills-price" role="tabpanel" aria-labelledby="pills-price-tab">
                        <div className="form-group">
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label">CAR PRICE* <span>(SAR)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="carprice" value={this.state.carprice} onChange={this.handleChange} onKeyPress={this.handleisNumberKey} placeholder="Enter" type="text" size="5" maxlength="9" minlength="9" required=""/>
                                </div>
                            </div>
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label">DOWN PAYMENT* <span>(%)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="downpayment" value={this.state.downpayment} onChange={this.handleChange} onKeyPress={this.handleisNumberKey} placeholder="Enter" type="text" size="5" maxlength="4" minlength="4" required=""/>
                                </div>
                            </div>
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label">TENURE* <span>(YEAR)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="tenure" value={this.state.tenure} onChange={this.handleChange} onKeyPress={this.handleisNumberKey} placeholder="Enter" type="text" size="5" maxlength="3" minlength="3" required=""/>
                                </div>
                            </div>
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label">INTEREST RATE* <span>(%)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="interestrate" value={this.state.interestrate} onChange={this.handleChange} onKeyPress={this.handleisNumberKey} placeholder="Enter" type="text" size="5" maxlength="4" minlength="4" required=""/>
                                </div>
                            </div>
                            <div className="pt-3">
                                <button type="submit" onClick={this.handlepriceonClick} id="btn_Calcualate" role="button" className="btn btn-block">Calculate</button>
                            </div>
                            <div><div>Monthly Installment</div></div><div><div>{this.state.priceMI}</div></div>
                            <div><div>Total Amount</div></div><div><div>{this.state.priceTA}</div></div>
                            <div><div>Total Interest Amount</div></div><div><div>{this.state.priceTIA}</div></div>

                        </div>
                    </div>
                    <div className="tab-pane fade" id="pills-emi" role="tabpanel" aria-labelledby="pills-emi-tab">
                        <div className="form-group">
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label" >EMI* <span >(SAR)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="emi" value={this.state.emi} onChange={this.handleChange} onChange={this.handleChange} placeholder="Enter" type="text" size="5" maxlength="9" minlength="9" required=""/>
                                </div>
                            </div>
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label">DOWN PAYMENT* <span>(%)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="emidownpayment" value={this.state.emidownpayment} onChange={this.handleChange} onChange={this.handleChange} placeholder="Enter" type="text" size="5" maxlength="4" minlength="4" required=""/>
                                </div>
                            </div>
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label">TENURE* <span>(YEAR)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="emitenure" value={this.state.emitenure} onChange={this.handleChange} onChange={this.handleChange} placeholder="Enter" type="text" size="5" maxlength="3" minlength="3" required=""/>
                                </div>
                            </div>
                            <div className="row my-3">
                                <div className="col-6 align-self-center px-0">
                                    <label className="control-label" >INTEREST RATE* <span >(%)</span></label>
                                </div>
                                <div className="col-6">
                                    <input className="form-control" name="emiinterestrate" value={this.state.emiinterestrate} onChange={this.handleChange} onChange={this.handleChange} placeholder="Enter" type="text" size="5" maxlength="4" minlength="4" required=""/>
                                </div>
                            </div>
                            <div className="pt-3">
                                <button type="submit" onClick={this.handleemionClick} id="btn_Calcualate2" role="button" className="btn btn-block">Calculate</button>
                            </div>
                            <div><div>Monthly Installment</div></div><div><div>{this.state.emiP}</div></div>
                            <div><div>Car Amount</div></div><div><div>{this.state.emiPA}</div></div>
                            <div><div>Total Amount</div></div><div><div>{this.state.emiTA}</div></div>
                            <div><div>Total Interest Amount</div></div><div><div>{this.state.emiTIA}</div></div>
                        </div>  
                    </div>
                
            </div>
        );
    }
}

export default Financingmainleft;