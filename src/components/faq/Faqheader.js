import React, { Component } from 'react';

class Faqheader extends Component {
    render() {
        return (
            <div>
                 <div className="container-fluid" style={{backgroundColor: '#646464', padding: '60px 50px 40px 50px'}}>
                    <div className="col-12 d-flex justify-content-center">
                    <span style={{fontSize: '30px', fontWeight: 800, color: 'white'}}>Frequently Asked Questions</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Faqheader;