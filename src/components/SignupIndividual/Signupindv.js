import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class Signupindv extends Component {
    render() {
        return (
            <div>
              <div className="logmod__wrapper">
                 <div className="logmod__container">
                 <ul className="row">
                <div className="col-12">
                    
                        <div className="row mb-5">
                    <div className="col-6"><span className="float-signin">Sign In</span></div> 
                    <div className="col-6"><span className="signreg">Register</span></div> 
                    </div>
                    
                    </div>
               
              </ul>
                <div className="logmod__tab lgm-4 show">
                  <div className="logmod__heading mt-3">
                    <span className="logmod__heading-subtitle">Sign up with your favourite social network
                    </span></div>
                  <div className="logmod__alter-container">
                    <div className="social-login">
                      <button className="btn facebook-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-facebook-f" /> </span> </button>
                      <button className="btn twitter-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-twitter" /></span> </button>
                      <button className="btn linkedin-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-linkedin" /></span> </button>
                      <button className="btn instagram-btn social-btn social-shadow" type="button"><span className="d-flex justify-content-center"><i className="fab fa-instagram" /></span> </button>
                    </div>
                  </div>
                  <div className="logmod__heading">
                    <div className="OR"> <span>OR</span></div>
                    <hr style={{margin: '0px'}} /> 
                  </div> 
                  <div>
                    <span className="logmod__heading-subtitlee">Register with Email</span>
                  </div>
                  <div className="logmod__form">
                    <div className="container mt-3 border-bottom">
                      <label className="string optional" style={{fontWeight: 700, fontSize: '12px'}}>Select Gender *</label>
                      <div id="radios">
                        <label htmlFor="Male" className="material-icons">
                          <input id="Male" name="User.Gender" required="required" type="radio" defaultValue="" />
                          <span className="outline-orange"><i className="fas fa-male fa-2x icon-pos-y" /> &nbsp;Male</span>
                        </label>
                        <label htmlFor="Female" className="material-icons">
                          <input id="Female" name="User.Gender" required="required" type="radio" defaultValue="Female" />
                          <span className="outline-orange"><i className="fas fa-female fa-2x icon-pos-y" /> &nbsp;Female</span>
                        </label>
                      </div>
                    </div>
                    <form acceptCharset="utf-8" action="#" className="simform">
                      <div className="sminputs">
                        <div className="input string optional">
                          <label className="string optional" htmlFor="user-name">First Name *</label>
                          <input className="string optional" maxLength={255} id="user-firstname" placeholder="First Name" type="text" size={50} />
                        </div>
                        <div className="input string optional">
                          <label className="string optional" htmlFor="user-name">Last Name *</label>
                          <input className="string optional" maxLength={255} id="user-lastname" placeholder="Last Name" type="text" size={50} />
                          <label htmlFor className="text-danger" />
                        </div>
                      </div>
                      <div className="sminputs">
                        <div className="input full">
                          <label className="string optional" htmlFor="user-name">Email*</label>
                          <input className="string optional" maxLength={255} id="user-email" placeholder="Email" type="email" size={50} />
                        </div>
                      </div>
                      <div className="sminputs">
                        <div className="input full">
                          <label className="string optional " htmlFor="user-name">Mobile Phone Number</label>
                          <div className="row">
                            <select className="custom-select" disabled>
                              <option selected>966</option>
                            
                            </select>
                            <input className="string optional mobile pl-2" maxLength={255} id="user-mobile" placeholder="   Enter your Mobile Number" type="text" size={15} />
                          </div>
                          <br />
                        </div>
                      </div>
                      <div className="sminputs password">
                        <div className="input string optional">
                          <label className="string optional" htmlFor="user-pw">Password *</label>
                          <input className="string optional" maxLength={255} id="user-pw" placeholder="Password" type="text" size={50} onFocus="showHideDiv('password-help'); " onFocusout="showHideDiv('password-help');" />
                          <span className="hide-password"><i className="fas fa-eye" /></span>
                        </div>
                        <div id="password-help" className="helper-text-box">
                          <ul className="helper-text">
                            <li className="helper-text-item"><i className="fas fa-circle pr-1" style={{fontSize: '0.4em', color: '#ff5500'}} />Must include at least 8 characters</li>
                            <li className="helper-text-item"><i className="fas fa-circle pr-1" style={{fontSize: '0.4em', color: '#ff5500'}} />Must include at least 1 uppercase letter (A-Z)</li>
                            <li className="helper-text-item"><i className="fas fa-circle pr-1" style={{fontSize: '0.4em', color: '#ff5500'}} />Must include at least 1 lowercase letter (a-z)</li>
                            <li className="helper-text-item"><i className="fas fa-circle pr-1" style={{fontSize: '0.4em', color: '#ff5500'}} />Must include at least 1 numerical digit (0-9)</li>
                            <li className="helper-text-item"><i className="fas fa-circle pr-1" style={{fontSize: '0.4em', color: '#ff5500'}} />Must include at least 1 special character (!@#$%^*)</li>
                          </ul>
                        </div>
                        <div className="input string optional">
                          <label className="string optional" htmlFor="user-pw-repeat">Confirm password *</label>
                          <input className="string optional" maxLength={255} id="user-pw-repeat" placeholder="Confirm password" type="text" size={50} />
                          <span className="hide-password"><i className="fas fa-eye" /></span>
                        </div>
                      </div>
                     
                        <div>
                            <Link to="/OTPRegindv" className="btn btn-block btn-dark-blue mt-3" style={{marginLeft: '0px'}}>SIGN UP</Link>
                          </div>
                   
                    </form>
                  </div> 
                  <div className="logmod__alter mt-5">
                    We value your privacy.
                    <br />
                    Cararabiya.com <Link className="special" to="/PrivacyPolicy">Privacy Statement.</Link>
                  </div>
                </div> 
            </div>
            </div>
            </div>
        )
    }
}
