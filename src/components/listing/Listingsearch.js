import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './listingsearch.css';

class Listingsearch extends Component {
    constructor(props)
    {
        super(props);
        this.state = { smake: '', smodel: '', sversion: '', scity: '', lmake: [], lmodel: [], lversion: [], lcity: [], error: null, validationError: '' };
    }
    
    componentDidMount() {
        const apiUrlMake = 'http://cararabiya.ahalfa.com/api/make';
        const apiUrlCity = 'http://cararabiya.ahalfa.com/api/city?countryID=184';
        
    
        fetch(apiUrlMake)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                lbrand: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )

          fetch(apiUrlCity)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                lcity: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )
      }
    render() {
        return (
            <div>

<div className="card">
              <div className="card-header px-0 py-0" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <h3 className="widget-title" style={{backgroundColor: '#e8e8e8', color: '#253241', textTransform: 'none', fontSize: '15px', fontWeight: 600}}>Quick Search <button><i class="fas fa-redo-alt"></i></button></h3>
              </div>
              <div className="accordion-body" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion-2">
              <div className="card-body">
                  <div className="widget-inner" style={{paddingTop: '16px'}}>
                    <div className="label-group mb-3">
                      <input className="forms__radio hidden" id="forms__radio-4" type="radio" name="radio-group-3" defaultValue defaultChecked="checked" />
                      <label className="forms__label forms__label-radio forms__label-radio-2 mr-2" htmlFor="forms__radio-4" style={{fontSize: '0.9em'}}>New Cars</label>
                      <input className="forms__radio hidden" id="forms__radio-3" type="radio" name="radio-group-3" defaultValue />
                      <label className="forms__label forms__label-radio forms__label-radio-2" htmlFor="forms__radio-3" style={{fontSize: '0.9em'}}>Used Cars</label>
                    </div>
                    <form className="b-filter bg-light">
                      <div className="b-filter__main">
                        <div className="b-filter__row">
                          <select className="selectpicker" data-width="100%" title="Select Make" multiple="multiple" data-max-options={1} data-style="quick-select" data-show-subtext="true" data-live-search="true">
                            <option>Select 1</option>
                            <option>Select 2</option>
                            <option>Select 3</option>
                            <option>Select 4</option>
                          </select>
                        </div>
                        <div className="b-filter__row">
                          <select className="selectpicker" data-width="100%" title="Select Model" multiple="multiple" data-max-options={1} data-style="quick-select" data-show-subtext="true" data-live-search="true">
                            <option>Select 1</option>
                            <option>Select 2</option>
                            <option>Select 3</option>
                            <option>Select 4</option>
                          </select>
                        </div>
                        <div className="b-filter__row">
                          <select className="selectpicker" data-width="100%" title="Select Year" multiple="multiple" data-max-options={1} data-style="quick-select" data-show-subtext="true" data-live-search="true">
                            <option>Select 1</option>
                            <option>Select 2</option>
                            <option>Select 3</option>
                            <option>Select 4</option>
                          </select>
                        </div>
                        <div className="b-filter__row">
                          <select className="selectpicker" data-width="100%" title="Select City" multiple="multiple" data-max-options={1} data-style="quick-select" data-show-subtext="true" data-live-search="true">
                            <option>Select 1</option>
                            <option>Select 2</option>
                            <option>Select 3</option>
                            <option>Select 4</option>
                          </select>
                        </div>
                        <div className="b-filter-slider ui-filter-slider">
                          <div className="b-filter-slider__title">Price</div>
                          <div className="b-filter-slider__main">
                            <div id="filterPrice" />
                            <div className="b-filter__row row">
                              <div className="b-filter__item col-md-6 col-lg-12 col-xl-6">
                                <input className="quick-select" id="input-with-keypress-0" />
                              </div>
                              <div className="b-filter__item col-md-6 col-lg-12 col-xl-6">
                                <input className="quick-select" id="input-with-keypress-1" />
                              </div>
                            </div>
                          </div>
                        </div>    
                      </div>
                      <div id="special">
                        <a href="#" className="btn btn-primary btn-block">Search</a>
                      </div>
                    </form>
                  </div>
                  <div className="widget-content">
                    {/* quick search widget inner close */}
                    <div className="widget-advance advance-search"> {/* advance search widget inner start */}
                      <div className="accordion" id="accordion-1">
                        <div className="card">
                          <div className="card-header" id="headingOne">
                            <Link className="accordion-trigger ui-subtitle justify-content-center collapsed ml-3" type="button" to="/Advancesearch">Advance search</Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div> 

  
            </div>
        );
    }
}

export default Listingsearch;