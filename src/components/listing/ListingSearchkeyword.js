import React, { Component } from 'react'

export default class ListingSearchkeyword extends Component {
    render() {
        return (
            <div>
                <div className="widget widget-search section-sidebar">
            <div className="b-filter-slider__title">Search by keyword</div>
            <form className="form-sidebar-adv" id="search-global-form">
              <input className="form-sidebar-adv__input form-control" type="search" placeholder="keyword" />
              <button className="form-sidebar-adv__btn"><i className="ic icon-magnifier" /></button>
            </form>
          </div>
            </div>
        )
    }
}
