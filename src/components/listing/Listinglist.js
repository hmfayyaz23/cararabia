import React, { Component } from 'react';
import Listinglatestadstop from './Listinglatestadstop';
import Listingadvertisementmiddle from './Listingadvertisementmiddle';


class Listinglist extends Component {
    render() {
        return (
            <div>
             
        <div className="b-filter-goods pb-0">
          <div className="row justify-content-between align-items-center">
            <div className="b-filter-goods__info col-auto">Showing results<strong> 1 to 10</strong> of total<strong> 145</strong></div>
          </div>
        </div>
            <Listinglatestadstop/>
        <div className="col-12 line-o my-3 px-0"> {/* Listing start
 */}
          <div className="row">
            <div className="col-lg-8 px-0">
              <div className="row">
                <div className="col-12 px-0 pl-3 mt-2 mb-3">
                  <div className="d-flex align-items-center Ad-top-sec">
                    <i className="fas fa-star mr-2" /><span className="mr-3">Featured ad</span>
                    <i className="fas fa-bolt mr-2" /><span className="mr-3">Super hot</span>
                    <button type="submit" className="btn btn-sm liketoggle like Ad-like-btn-style" name="like"> <i className="fas fa-heart" /> <span>Save</span></button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-5">
                  <div className="b-goods-f__listing mb-2">
                    <a href="Detailed-Ad.html">
                      <img className="watermark-listing" src="./assets/media/watermark.png" />
                      <img className="b-goods-f__img img-scale" src="./assets/media/content/b-goods/300x220/2.jpg" alt="" /></a></div>
                  <span className="Ad-location"><i className="fas fa-map-marker-alt" /> ABC road, Saudi Arabia
                  </span>
                </div>
                <div className="col-12 col-md-7">
                  <div className="b-goods-f__main d-flex">
                    <div className="b-goods-f__descrip">
                      <div className="b-goods-f__title_myad"><a href="Detailed-Ad.html">Honda Vezel</a></div>
                      <div className="b-goods-f__info Ad-posted-date">6 September 2019</div>
                      <ul className="b-goods-f__list list-unstyled mt-1 fsize-car-descr">
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Mileage :</span><span className="b-goods-f__list-info Ad-mileage">35,000km</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Model :</span><span className="b-goods-f__list-info Ad-Model">2019</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Transmission :</span><span className="b-goods-f__list-info Ad-Transmission">Manual</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Color :</span><span className="b-goods-f__list-info Ad-fuel">Black</span></li>
                      </ul>
                    </div>
                  </div>
                  <div className="Ad-email-listing">
                    <a style={{color: 'grey'}} href="#"><span className="email-font-size mr-3"><i className="far fa-envelope mr-1" /> Email this listing</span></a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12 px-0 list-sec-r"> {/* price section */}
              <div className="Ad-price-sec"> 
                <span className="Ad-price-style">SAR &nbsp; 50,000</span>
              </div>
              <div> {/* buttons */}
                <div className="b-goods-f__sidebar px-3">
                  <span className="b-goods-f__price-group">
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center Ad-btn-grey"> <i className="fas fa-phone prefix justify-content-between" /><span className="Ad-btn-grey-s">Show Phone Number</span></button>
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"> <i className="fas fa-envelope prefix justify-content-start" /><span className="Ad-btn-grey-s">Show Email</span></button>
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"> <i className="far fa-comment-dots prefix justify-content-center" /><span className="Ad-btn-grey-s">Chat with seller</span>
                    </button>
                  </span> 
                  <span className="b-goods-f__compare">
                    <div className="form-check d-flex align-self-center">
                      <input className="form-check-input Atocompare-checkbox " type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                      <label className="form-check-label Atocompare-text">Add to compare</label>
                    </div>
                  </span>
                </div>
              </div>
            </div>
          </div> {/* end row */}
        </div>  {/* listing end */} 
        <div className="col-12 line-o my-3 px-0 bg-shadow"> {/* Listing start
 */}
          <div className="row">
            <div className="col-lg-8 px-0">
              <div className="row">
                <div className="col-12 px-0 pl-3 mt-2 mb-3">
                  <div className="d-flex align-items-center Ad-top-sec">
                    <i className="fas fa-star mr-2" /><span className="mr-3">Featured ad</span>
                    <i className="fas fa-bolt mr-2" /><span className="mr-3">Super hot</span>
                    <button type="submit" className="btn btn-sm liketoggle like Ad-like-btn-style" name="like"> <i className="fas fa-heart" /> <span>Save</span></button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-5">
                  <div className="b-goods-f__listing mb-2">
                    <a href="Detailed-Ad.html">
                      <img className="watermark-listing" src="./assets/media/watermark.png" />
                      <img className="b-goods-f__img img-scale" src="./assets/media/content/b-goods/300x220/2.jpg" alt="" /></a></div>
                  <span className="Ad-location"><i className="fas fa-map-marker-alt" /> ABC road, Saudi Arabia
                  </span>
                </div>
                <div className="col-12 col-md-7">
                  <div className="b-goods-f__main d-flex">
                    <div className="b-goods-f__descrip">
                      <div className="b-goods-f__title_myad"><a href="Detailed-Ad.html">Honda Vezel</a></div>
                      <div className="b-goods-f__info Ad-posted-date">6 September 2019</div>
                      <ul className="b-goods-f__list list-unstyled mt-1 fsize-car-descr">
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Mileage :</span><span className="b-goods-f__list-info Ad-mileage">35,000km</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Model :</span><span className="b-goods-f__list-info Ad-Model">2019</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Transmission :</span><span className="b-goods-f__list-info Ad-Transmission">Manual</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Color :</span><span className="b-goods-f__list-info Ad-fuel">Black</span></li>
                      </ul>
                    </div>
                  </div>
                  <div className="Ad-email-listing">
                    <a style={{color: 'grey'}} href="#"><span className="email-font-size mr-3"><i className="far fa-envelope mr-1" /> Email this listing</span></a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12 px-0 list-sec-r"> {/* price section */}
              <div className="Ad-price-sec"> 
                <span className="Ad-price-style">SAR &nbsp; 50,000</span>
              </div>
              <div> {/* buttons */}
                <div className="b-goods-f__sidebar px-3">
                  <span className="b-goods-f__price-group">
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center Ad-btn-grey"> <i className="fas fa-phone prefix justify-content-between" /><span className="Ad-btn-grey-s">Show Phone Number</span></button>
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"> <i className="fas fa-envelope prefix justify-content-start" /><span className="Ad-btn-grey-s">Show Email</span></button>
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"> <i className="far fa-comment-dots prefix justify-content-center" /><span className="Ad-btn-grey-s">Chat with seller</span>
                    </button>
                  </span> 
                  <span className="b-goods-f__compare">
                    <div className="form-check d-flex align-self-center">
                      <input className="form-check-input Atocompare-checkbox " type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                      <label className="form-check-label Atocompare-text">Add to compare</label>
                    </div>
                  </span>
                </div>
              </div>
            </div>
          </div> {/* end row */}
        </div>  {/* listing end */} 
        <div className="col-12 line-o my-3 px-0"> {/* Listing start
 */}
          <div className="row">
            <div className="col-lg-8 px-0">
              <div className="row">
                <div className="col-12 px-0 pl-3 mt-2 mb-3">
                  <div className="d-flex align-items-center Ad-top-sec">
                    <i className="fas fa-star mr-2" /><span className="mr-3">Featured ad</span>
                    <i className="fas fa-bolt mr-2" /><span className="mr-3">Super hot</span>
                    <button type="submit" className="btn btn-sm liketoggle like Ad-like-btn-style" name="like"> <i className="fas fa-heart" /> <span>Save</span></button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-5">
                  <div className="b-goods-f__listing mb-2">
                    <a href="Detailed-Ad.html">
                      <img className="watermark-listing" src="./assets/media/watermark.png" />
                      <img className="b-goods-f__img img-scale" src="./assets/media/content/b-goods/300x220/2.jpg" alt="" /></a></div>
                  <span className="Ad-location"><i className="fas fa-map-marker-alt" /> ABC road, Saudi Arabia
                  </span>
                </div>
                <div className="col-12 col-md-7">
                  <div className="b-goods-f__main d-flex">
                    <div className="b-goods-f__descrip">
                      <div className="b-goods-f__title_myad"><a href="Detailed-Ad.html">Honda Vezel</a></div>
                      <div className="b-goods-f__info Ad-posted-date">6 September 2019</div>
                      <ul className="b-goods-f__list list-unstyled mt-1 fsize-car-descr">
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Mileage :</span><span className="b-goods-f__list-info Ad-mileage">35,000km</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Model :</span><span className="b-goods-f__list-info Ad-Model">2019</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Transmission :</span><span className="b-goods-f__list-info Ad-Transmission">Manual</span></li>
                        <li className="b-goods-f__list-item"><span className="b-goods-f__list-title">Color :</span><span className="b-goods-f__list-info Ad-fuel">Black</span></li>
                      </ul>
                    </div>
                  </div>
                  <div className="Ad-email-listing">
                    <a style={{color: 'grey'}} href="#"><span className="email-font-size mr-3"><i className="far fa-envelope mr-1" /> Email this listing</span></a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12 px-0 list-sec-r"> {/* price section */}
              <div className="Ad-price-sec"> 
                <span className="Ad-price-style">SAR &nbsp; 50,000</span>
              </div>
              <div> {/* buttons */}
                <div className="b-goods-f__sidebar px-3">
                  <span className="b-goods-f__price-group">
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center Ad-btn-grey"> <i className="fas fa-phone prefix justify-content-between" /><span className="Ad-btn-grey-s">Show Phone Number</span></button>
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"> <i className="fas fa-envelope prefix justify-content-start" /><span className="Ad-btn-grey-s">Show Email</span></button>
                    <button type="button" className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"> <i className="far fa-comment-dots prefix justify-content-center" /><span className="Ad-btn-grey-s">Chat with seller</span>
                    </button>
                  </span> 
                  <span className="b-goods-f__compare">
                    <div className="form-check d-flex align-self-center">
                      <input className="form-check-input Atocompare-checkbox " type="checkbox" id="inlineCheckbox1" defaultValue={1} />
                      <label className="form-check-label Atocompare-text">Add to compare</label>
                    </div>
                  </span>
                </div>
              </div>
            </div>
          </div> {/* end row */}
        </div>  {/* listing end */} 
          <Listingadvertisementmiddle/>
        <nav aria-label="Page navigation" className="mb-5">
          <ul className="pagination justify-content-center">
            <li className="page-item active" aria-current="page"><a className="page-link" href="#" style={{backgroundColor: 'transparent', color: '#ff5500'}}>1</a></li>
            <li className="page-item"><a className="page-link" href="#" style={{border: 'none'}}>2</a></li>
            <li className="page-item"><a className="page-link" href="#" style={{border: 'none'}}>3</a></li>
            <li className="page-item"><a className="page-link" href="#" style={{border: 'none'}}>4</a></li>
            <li className="page-item"><a className="page-link" href="#" style={{border: 'none'}}>5</a></li>
            <li className="page-item"><a className="page-link" href="#" aria-label="Next" style={{border: 'none'}}><span className="ic fas fa-angle-double-right" aria-hidden="true" /></a></li>
          </ul>
        </nav>
      
            </div>
        );
    }
}

export default Listinglist;