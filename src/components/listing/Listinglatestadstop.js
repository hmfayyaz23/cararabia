import React, { Component } from 'react';

class Listinglatestadstop extends Component {
    render() {
        return (
            <div>
               <main className="b-goods-group row feature-toggle-top">
          <div className="b-goods-f col-lg-4 col-md-6">
            <div className="b-goods-f__media">
            <img className="watermark-recent" src="./assets/media/watermark.png" />
              <img className="b-goods-f__img img-scale" src="./assets/media/content/b-goods/300x220/1.jpg" alt="foto" /></div>
            <div className="b-goods-f__main bg-shadow">
              <div className="b-goods-f__descrip">
                <div className="b-goods-f__title b-goods-f__title_myad" style={{marginBottom: '0px'}}>
                  <a style={{fontSize: '18px', fontWeight: 600}} href="Detailed-Ad.html">Audi L35 Quattro</a>
                </div>
               
                <ul className="b-goods-f__list list-unstyled row justify-content-between" style={{fontSize: '11px', padding: '5px 20px', marginTop: '0px'}}>
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-calendar-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>2019</span>
                      </div>
                    </div>
                  </div>   
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-tachometer-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>5.6 Km/Ltr</span>
                      </div>
                    </div>
                  </div>   
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-car-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>Used</span>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div>
                      <div>
                        <i className="fas fa-gas-pump" style={{color: '#ff5500', fontSize: '12px'}} />
                      </div>
                      <div>
                        <span className="b-goods-f__list-info" style={{color: 'black'}}>250 Ltr</span>
                      </div>
                    </div>
                  </div>     
                </ul>
              </div>
              <div className="b-goods-f__sidebar" style={{paddingTop: '10px', borderTop: '1px solid #ddd'}}><a className="b-goods-f__bnr" href="#"><img src="./assets/media/content/b-goods/auto-check.png" alt="auto check" /></a><span className="b-goods-f__price-group"><span className="b-goods-f__pricee"><span className="b-goods-f__price_col">PRICE:&nbsp;</span><span className="b-goods-f__price-numbb">SAR 60,000</span></span></span>   
              </div>
            </div>
          </div>
          {/* end .b-goods*/}
          <div className="b-goods-f col-lg-4 col-md-6">
            
          </div>
          <div className="b-goods-f col-lg-4 col-md-6">
           
          </div>
        </main>
            </div>
        );
    }
}

export default Listinglatestadstop;