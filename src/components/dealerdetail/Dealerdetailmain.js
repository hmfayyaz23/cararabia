import React, { Component } from 'react';

class Dealerdetailmain extends Component {
    constructor(props)
    {
        super(props);
        this.state = { 
            dealerid: this.props.id, 
            datadealer: [],
            documentdealer: [], 
            error: null, 
            validationError: '' };
        
    }
    componentDidMount () {
        const apiUrlDealer = 'http://cararabiya.ahalfa.com/api/Dealer/'+ this.state.dealerid;
        const apiUrldocDealer = 'http://cararabiya.ahalfa.com/api/DealerDocument/GetDealerDocumentByDealerID?dealerid='+ this.state.dealerid;

        fetch(apiUrlDealer)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                datadealer: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )

          fetch(apiUrldocDealer)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                documentdealer: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )
    }
    render() {
        const imageheader='http://cararabiyaweb.ahalfa.com/Content/assets/media/dealer-main/abdullah/1.jpg';
        return (
            <div>
                {this.state.datadealer.map(dealer => (
                <div>
                <div className="b-dealers-info__header">
                    <div className="row justify-content-between align-items-center">
                    <div className="col">
                        <h2 className="b-dealers-info__title ui-title-inner">{dealer.dealerName}</h2>
                    </div>
                    <div className="col-auto">
                        <span className="b-dealers-info__brand b-dealers__brand">
                            <img className="b-dealers-info__brand-img img-fluid" src={dealer.dealerLogoURL} alt="brand" style={{maxWidth: '46px', maxHeight: '88px'}} />
                        </span>
                    </div>
                    </div>
                </div>
                <div className="entry-media">
                    <a className="js-zoom-images" href="http://cararabiya.ahalfa.com/Content/assets/media/dealer-main/abdullah/1.jpg">
                        <img className="img-fluid" src={imageheader} alt="Foto" />
                    </a>
                </div>
                <div className="b-dealers-info__desrip">
                    <h2 className="ui-title-sm">About Us</h2>
                <p>{dealer.AboutUs}</p>
                </div>
                <div className="row mt-5">
                    <div className="b-dealers-info__contacts col-lg">
                    <div className="b-dealers-info__contacts-item">
                        <i className="ic icon-location-pin text-primary" />
                        <div className="b-dealers-info__contacts-title mb-1">Location</div>
                        <div className="b-dealers-info__contacts-info">{dealer.addressLine1} , {dealer.addressLine2} , {dealer.cityName}</div>
                    </div>
                    <div className="b-dealers-info__contacts-item">
                        <i className="ic icon-call-in text-primary" />
                        <div className="b-dealers-info__contacts-title mb-1">Dealer Phone</div>
                        <div className="b-dealers-info__contacts-info">{dealer.contactNumber}</div>
                    </div>
                    </div>
                    <div className="col-lg-auto">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d761.8102551300179!2d46.80129770611083!3d24.73372149214452!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7222154799a6f0c2!2sFahd%20Abdullah%20Al%20Dosari%20Motor%20Show!5e0!3m2!1sen!2sus!4v1577448055993!5m2!1sen!2sus" width={450} height={280} frameBorder={0} style={{border: 0}} allowFullScreen />
                    </div>
                </div>
                <div class="b-dealers-info__desrip mb-3">
                        <h2 class="ui-title-sm">Documents</h2>

                </div>
                <div class="row">
                {this.state.documentdealer.map(doc => (
                    <div>
                           <div className="col-12 col-lg-3">
                                        <div className="entry-media">
                                            <a className="js-zoom-images" href={doc.dealerDocURL}>
                                                <img className="img-fluid img-thumbnail" src={doc.dealerDocURL} alt="Foto"/>
                                            </a>
                                        </div>
                                    </div>
                    </div>
                ))}
                    </div>
                </div>
                    ))}
            </div>
        );
    }
}

export default Dealerdetailmain;