import React, { Component } from 'react';

class Dealerdetailadsbyuser extends Component {
    render() {
        const image1='http://cararabiyaweb.ahalfa.com/Content/assets/media/content/b-goods/300x220/1.jpg';
        const image2='http://cararabiyaweb.ahalfa.com/Content/assets/media/content/b-goods/auto-check.png';
        return (
            <div>
                <div className="b-dealers-info__desrip mb-3">
                    <h2 className="ui-title-sm">Ads by Us</h2>
                </div>
                <main className="b-goods-group row">
                    <div className="b-goods-f col-lg-4 col-md-6">
                    <div className="b-goods-f__media">
                        <img className="b-goods-f__img img-scale" src={image1} alt="foto" />
                            <span className="b-goods-f__media-inner">
                                <span className="b-goods-f__favorite">
                                    <i className="ic far fa-star" />
                                </span>
                                <span className="b-goods-f__label bg-primary">Latest</span>
                            </span>
                    </div>
                    <div className="b-goods-f__main bg-shadow">
                        <div className="b-goods-f__descrip">
                        <div className="b-goods-f__title b-goods-f__title_myad" style={{marginBottom: '0px'}}>
                            <a style={{fontSize: '18px', fontWeight: 600}} href="#">Audi L35 Quattro</a>
                        </div>
                        <div className="b-goods-f__info">Magna aliqua enim aduas veniam quis nostrud exercitation ullam laboris aliquip.</div>
                        <ul className="b-goods-f__list list-unstyled row justify-content-between" style={{fontSize: '11px', padding: '5px 20px', marginTop: '0px'}}>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-calendar-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>2019</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-tachometer-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>5.6 Km/Ltr</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-car-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>Used</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-gas-pump" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>250 Ltr</span>
                                </div>
                            </div>
                            </div>
                        </ul>
                        </div>
                        <div className="b-goods-f__sidebar" style={{paddingTop: '10px', borderTop: '1px solid #ddd'}}>
                        <a className="b-goods-f__bnr" href="#"><img src={image2} alt="auto check" /></a><span className="b-goods-f__price-group"><span className="b-goods-f__pricee"><span className="b-goods-f__price_col">PRICE:&nbsp;</span><span className="b-goods-f__price-numbb">SAR 60,000</span></span></span>
                        </div>
                    </div>
                    </div>
                    {/* end .b-goods*/}
                    <div className="b-goods-f col-lg-4 col-md-6">
                    <div className="b-goods-f__media"><img className="b-goods-f__img img-scale" src={image1} alt="foto" /><span className="b-goods-f__media-inner"><span className="b-goods-f__favorite"><i className="ic far fa-star" /></span><span className="b-goods-f__label bg-primary">Latest</span></span></div>
                    <div className="b-goods-f__main bg-shadow">
                        <div className="b-goods-f__descrip">
                        <div className="b-goods-f__title b-goods-f__title_myad" style={{marginBottom: '0px'}}>
                            <a style={{fontSize: '18px', fontWeight: 600}} href="#">Audi L35 Quattro</a>
                        </div>
                        <div className="b-goods-f__info">Magna aliqua enim aduas veniam quis nostrud exercitation ullam laboris aliquip.</div>
                        <ul className="b-goods-f__list list-unstyled row justify-content-between" style={{fontSize: '11px', padding: '5px 20px', marginTop: '0px'}}>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-calendar-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>2019</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-tachometer-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>5.6 Km/Ltr</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-car-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>Used</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-gas-pump" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>250 Ltr</span>
                                </div>
                            </div>
                            </div>
                        </ul>
                        </div>
                        <div className="b-goods-f__sidebar" style={{paddingTop: '10px', borderTop: '1px solid #ddd'}}>
                        <a className="b-goods-f__bnr" href="#"><img src={image2} alt="auto check" /></a><span className="b-goods-f__price-group"><span className="b-goods-f__pricee"><span className="b-goods-f__price_col">PRICE:&nbsp;</span><span className="b-goods-f__price-numbb">SAR 60,000</span></span></span>
                        </div>
                    </div>
                    </div>
                    <div className="b-goods-f col-lg-4 col-md-6">
                    <div className="b-goods-f__media"><img className="b-goods-f__img img-scale" src={image1} alt="foto" /><span className="b-goods-f__media-inner"><span className="b-goods-f__favorite"><i className="ic far fa-star" /></span><span className="b-goods-f__label bg-primary">Latest</span></span></div>
                    <div className="b-goods-f__main bg-shadow">
                        <div className="b-goods-f__descrip">
                        <div className="b-goods-f__title b-goods-f__title_myad" style={{marginBottom: '0px'}}>
                            <a style={{fontSize: '18px', fontWeight: 600}} href="#">Audi L35 Quattro</a>
                        </div>
                        <div className="b-goods-f__info">Magna aliqua enim aduas veniam quis nostrud exercitation ullam laboris aliquip.</div>
                        <ul className="b-goods-f__list list-unstyled row justify-content-between" style={{fontSize: '11px', padding: '5px 20px', marginTop: '0px'}}>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-calendar-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>2019</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-tachometer-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>5.6 Km/Ltr</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-car-alt" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>Used</span>
                                </div>
                            </div>
                            </div>
                            <div>
                            <div>
                                <div>
                                <i className="fas fa-gas-pump" style={{color: '#ff5500', fontSize: '12px'}} />
                                </div>
                                <div>
                                <span className="b-goods-f__list-info" style={{color: 'black'}}>250 Ltr</span>
                                </div>
                            </div>
                            </div>
                        </ul>
                        </div>
                        <div className="b-goods-f__sidebar" style={{paddingTop: '10px', borderTop: '1px solid #ddd'}}>
                        <a className="b-goods-f__bnr" href="#"><img src={image2} alt="auto check" /></a><span className="b-goods-f__price-group"><span className="b-goods-f__pricee"><span className="b-goods-f__price_col">PRICE:&nbsp;</span><span className="b-goods-f__price-numbb">SAR 60,000</span></span></span>
                        </div>
                    </div>
                    </div>
                </main>
            </div>
        );
    }
}

export default Dealerdetailadsbyuser;