import React, { Component } from 'react';

class Dealerdetailbranches extends Component {
    constructor(props)
    {
        super(props);
        this.state = { 
            dealerid: this.props.id, 
            dealerbranches: [],
            error: null, 
            validationError: '' };
        
    }

    componentDidMount () {
        const apiUrlDealerBranch = 'http://cararabiya.ahalfa.com/api/DBranch/GetDBranchbyDealerID?DealerID='+ this.state.dealerid;
        

        fetch(apiUrlDealerBranch)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                dealerbranches: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )
    }

    render() {
        return (
            <div>
                 <div className="widget widget-search section-sidebar bg-light">
                    <div className="widget-title bg-cararabiya">Branches</div>
                    <div className="widget-inner">
                {this.state.dealerbranches.map(branch => (
                    <div>
                        
                    <div className="b-dealers-info__contacts col-lg mb-4">
                <div className="b-dealers-info__contacts-title mb-1">{branch.dBranchName}</div>
                        <hr />
                        <div className="b-dealers-info__contacts-item" style={{paddingBottom: '10px', marginBottom: '10px'}}>
                        <i className="ic icon-location-pin text-primary" />
                        <div className="b-dealers-info__contacts-title mb-1" style={{fontSize: '13px', color: '#464646'}}>Location</div>
                        <div className="b-dealers-info__contacts-info" style={{fontSize: '13px'}}>{branch.addressLine1}, {branch.addressLine2} , {branch.cityName}</div>
                        </div>
                        <div className="b-dealers-info__contacts-item" style={{paddingBottom: '10px', marginBottom: '10px'}}>
                        <i className="ic icon-call-in text-primary" />
                        <div className="b-dealers-info__contacts-title mb-1" style={{fontSize: '13px'}}>Branch Phone</div>
                        <div className="b-dealers-info__contacts-info" style={{fontSize: '13px'}}>{branch.contactNumber}</div>
                        </div>
                    </div>
                    
                    
                    </div>
                ))}
                </div>
                </div>

                
            </div>
        );
    }
}

export default Dealerdetailbranches;