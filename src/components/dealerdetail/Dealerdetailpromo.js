import React, { Component } from 'react';

class Dealerdetailpromo extends Component {
    constructor(props)
    {
        super(props);
        this.state = { 
            dealerid: this.props.id, 
            dealerpromo: [],
            error: null, 
            validationError: '' };
        
    }

    componentDidMount () {
        const apiUrlDealerPromo = 'http://cararabiya.ahalfa.com/api/PromoAndOffer/PromoByDealerID?DealerID='+ this.state.dealerid;
        

        fetch(apiUrlDealerPromo)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                dealerpromo: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )

          let newDate = new Date() 
          let filteredArray = this.state.dealerpromo.filter(item => item.Valid_Till > newDate)
          this.setState({dealerpromo: filteredArray});
    }


    render() {
        
        return (
            <div>
                 <section className="widget section-sidebar bg-light mt-4">
                    <h3 className="widget-title bg-cararabiya ">My Promotions</h3>
                    <div className="widget-content">
                    <div className="widget-inner">
                    {this.state.dealerpromo.map(promo => (
                        <div>
                            <section className="post-widget clearfix">
                                                    <div className="post-widget__media"><a className="js-zoom-images" href={promo.ImageUrl}>
                                                        <img height="100px" width="100px" className="img-fluid" src={promo.ImageUrl} alt="foto" /></a></div>
                                                    <div class="post-widget__inner">
                                                        <h2 class="post-widget__title">
                                                            <a href="">
                                                              {promo.PromotionName}
                                                            </a>
                                                        </h2>
                                                        <div class="post-widget__date">
                    <time datetime={promo.Valid_Till}>{promo.Valid_Till}</time>
                                                        </div>
                                                    </div>
                                                    
                                                </section>
                        </div>
                    ))}
                        
                        
                        
                    </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Dealerdetailpromo;