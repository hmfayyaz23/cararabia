import React, { Component } from 'react';

class Dealersearch extends Component {
    constructor(props) {
        super(props);
        this.state = { searchkey: '', lmake: [], smake:[], lcity: [], scity:[], error: null, validationError: '' };

        this.handlesearchkeyChange = this.handlesearchkeyChange.bind(this);
        this.handlecityChange = this.handlecityChange.bind(this);
        this.handlesearch = this.handlesearch.bind(this);
        this.handlemakeChange = this.handlemakeChange.bind(this);
        this.handleqsearch = this.handleqsearch.bind(this);
    }
    handlesearchkeyChange(event) {
        const searchkeyname = event.target.name;
        const searchkeyvalue = event.target.value;
    
        this.setState({
          [searchkeyname]: searchkeyvalue
        })
      }

      handlecityChange(event) {
        const searchkeyid = event.target.id;
        const searchkeyvalue = event.target.value;
        const cheked= event.target.checked;
        //var joined = this.state.scity.concat('abc');
        //this.setState({ scity: joined });
        
        if(cheked == true)
        {
            var joined = this.state.scity;
            joined.push(searchkeyid);
            this.setState({ scity: joined });
        }
        else
        {
            let filteredArray = this.state.scity.filter(item => item !== searchkeyid)
            this.setState({scity: filteredArray});
        }    
     
      }

      handlemakeChange(event) {
        const searchkeyid = event.target.id;
        const searchkeyvalue = event.target.value;
        const cheked= event.target.checked;
        //var joined = this.state.scity.concat('abc');
        //this.setState({ scity: joined });
        
        if(cheked == true)
        {
            var joined = this.state.smake;
            joined.push(searchkeyid);
            this.setState({ smake: joined });
        }
        else
        {
            let filteredArray = this.state.smake.filter(item => item !== searchkeyid)
            this.setState({smake: filteredArray});
        }
        
        
     
      }

      handlesearch(event) {
        
      }

      handleqsearch(event) {
        
    }

      componentDidMount() {
        
        const apiUrlCity = 'http://cararabiya.ahalfa.com/api/city?countryID=184';
        const apiUrlMake = 'http://cararabiya.ahalfa.com/api/Make';
    
        
          fetch(apiUrlCity)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                lcity: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )

          fetch(apiUrlMake)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                lmake: result
              });
            },
            (error) => {
              this.setState({ error });
            }
          )


      }
    render() {
        return (
            <div>
                 <div className="widget widget-search section-sidebar ">
                    <div className="b-filter-slider__title">Search by keyword</div>
                    <input className="form-sidebar-adv__input form-control" name="searchkey" value={this.state.searchkey} onChange={this.handlesearchkeyChange} placeholder="Enter Keyword"/>                            
                    <button className="form-sidebar-adv__btn" onClick={this.handlesearch}><i className="ic icon-magnifier"></i>search</button>
                </div>
                <h3 className="widget-title">Quick Search</h3>   
                <div className="b-filter-slider__title">City</div>  
                {this.state.lcity.map(city => (
                    <div className="form-check form-check-inline col-12">
                        <input type="checkbox" id={city.CityID} value={city.CityName} onChange={this.handlecityChange}/>{city.CityName}
                    </div>
                ))}

                <div className="b-filter-slider__title">Make</div>  
                {this.state.lmake.map(make => (
                    <div className="form-check form-check-inline col-12">
                        <input type="checkbox" id={make.MakeID} value={make.MakeName} onChange={this.handlemakeChange}/>{make.MakeName}
                    </div>
                ))}

                <div className="">
                    <button className="form-sidebar-adv__btn" onClick={this.handleqsearch}>search</button>
                </div>

               
            </div>
        );
    }
}

export default Dealersearch;