import React, { Component } from 'react';

class Dealeradvertisement extends Component {
    render() {
            const image1="http://cararabiyaweb.ahalfa.com/Content/assets/media/content/ad/ad1.jpg";
            const image2="http://cararabiyaweb.ahalfa.com/Content/assets/media/content/ad/ad2.jpg";
            const image3="http://cararabiyaweb.ahalfa.com/Content/assets/media/content/ad/ad3.jpg";
            const image4="http://cararabiyaweb.ahalfa.com/Content/assets/media/content/ad/ad4.jpg";
        return (
            
            <div>
               <div className="widget section-sidebar bg-light">
                    <div className="widget-content">
                    {/* quick search widget inner close */}
                    </div> {/* advance search widget inner close */}
                    <div className="container-fluid advertise-advance" style={{backgroundColor: '#e9e9e9'}}>
                    <div className="container pt-3">
                        <div className="col-12"><h4 className="ui-title-ad">Advertisement</h4></div>
                        <div className="b-gallery js-slider" data-slick="{&quot;slidesToShow&quot;: 1, &quot;arrows&quot;: false, &quot;autoplay&quot;: true,  &quot;slidesToScroll&quot;: 2, &quot;rows&quot;: 1, &quot;responsive&quot;: [{&quot;breakpoint&quot;: 1400, &quot;settings&quot;: {&quot;slidesToShow&quot;: 1, &quot;slidesToScroll&quot;: 3}}, {&quot;breakpoint&quot;: 768, &quot;settings&quot;: {&quot;slidesToShow&quot;: 1, &quot;slidesToScroll&quot;: 1}}]}">
                        <div className="ad"><img src={image1} /></div>
                        <div className="ad"><img src={image2} /></div>
                        <div className="ad"><img src={image3} /></div>
                        <div className="ad"><img src={image4} /></div>
                        <div className="ad"><img src={image1} /></div>
                        <div className="ad"><img src={image2} /></div>
                        <div className="ad"><img src={image3} /></div>
                        <div className="ad"><img src={image4} /></div>
                        </div>
                        <br /><br />
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dealeradvertisement;